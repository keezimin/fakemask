@extends('layout.master')

@section('content')
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12">
            <div class="row flex-grow">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="custom-col-20">
                                    <div class="form-group">
                                        <label for="objects">Chọn khu vực</label>
                                        <select class="form-control" id="objects">
                                            <option value=""></option>
                                            @foreach($objects as $value)
                                                <option value="{{$value->id}}"
                                                        @if (request()->object_id == $value->id) selected @endif >{{$value->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="custom-col-20">
                                    <div class="form-group" style="display: flex;flex-direction: row;flex-wrap: wrap;">
                                        <label for="cameras">Chọn camera</label>
                                        <span style="margin-left: auto;flex: 1;justify-content: flex-end;display: flex;margin-right: 10px;">Tất cả</span>
                                        <input id="cameraList" type="checkbox" class="float-right form-check-input all" style="margin-left: auto; float: none !important; position: relative; width: auto;">
                                        <select id="camera_select" class="cameras form-control select-nultiple w-100"
                                                multiple="multiple" data-width="100%" name="cameras[]">

                                            @foreach($cameras as $value)
                                                <option value="{{$value->id}}"
                                                    @if (request()->camera_id == $value->id)
                                                        selected
                                                    @elseif ($checkAll && in_array($value->id,$checkAll))
                                                        selected
                                                    @endif
                                                >
                                                    {{$value->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="custom-col-20">
                                    <div class="form-group">
                                        <label for="view_by">Xem theo</label>
                                        <select class="form-control" id="view_by">
                                            <option value='1' @if (request()->view_by == 1) selected @endif>Theo ngày
                                            </option>
                                            <option value='2' @if (request()->view_by == 2) selected @endif>Theo tháng
                                            </option>
                                            <option value='3' @if (request()->view_by == 3) selected @endif>Theo năm
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="custom-col-20">
                                    <div class="form-group">
                                        <label for="datePickerFrom">Chọn thời gian</label>
                                        <div class="input-group">
                                            <input id="view_time" class="form-control" value="{{request()->view_time}}" inputmode="numeric">
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col-20" style="margin-top: 29px;">
                                    <div class="form-group" style="width: 100%">
                                        <button type="button"
                                                class="btn-view-data btn btn-success btn-icon-text mb-2 mb-md-0"
                                                style="height: 36px; width: 100%">
                                            Xem
                                        </button>
                                        {{--<button type="button" class="btn-save-data btn btn-success btn-icon-text mb-2 mb-md-0" style="height: 36px; width: 139px;">--}}
                                        {{--Lưu biểu đồ--}}
                                        {{--</button>--}}
                                        {{--<button type="button" class="btn-export-data btn btn-danger btn-icon-text mb-2 mb-md-0" style="height: 36px; width: 139px;">--}}
                                        {{--Xuất dữ liệu--}}
                                        {{--</button>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    {{--<div class="col-md-3">--}}
                                        {{--{!! $pieChart->container() !!}--}}
                                        {{--<script>--}}
                                            {{--var options = {--}}
                                                {{--series: [45, 55],--}}
                                                {{--chart: {--}}
                                                    {{--width: 380,--}}
                                                    {{--type: 'pie',--}}
                                                {{--},--}}
                                                {{--colors: {!! $pieChart->colors() !!},--}}
                                                {{--labels: ['Đeo', 'Không đeo'],--}}
                                                {{--responsive: [{--}}
                                                    {{--breakpoint: 480,--}}
                                                    {{--options: {--}}
                                                        {{--chart: {--}}
                                                            {{--width: 200--}}
                                                        {{--},--}}
                                                        {{--legend: {--}}
                                                            {{--position: 'bottom'--}}
                                                        {{--}--}}
                                                    {{--}--}}
                                                {{--}]--}}
                                            {{--};--}}

                                            {{--var pieChart = new ApexCharts(document.querySelector("#{!! $pieChart->id() !!}"), options);--}}
                                            {{--pieChart.render();--}}
                                        {{--</script>--}}
                                        {{--{{ $pieChart->script() }}--}}
                                    {{--</div>--}}
                                    <div class="chart-analytics">
                                        {{--@if(!empty($chart))--}}
                                            {{--{!! $chart->container() !!}--}}
                                            {{--<script>--}}
                                                {{--var options =--}}
                                                    {{--{--}}
                                                        {{--chart: {--}}
                                                            {{--type: '{!! $chart->type() !!}',--}}
                                                            {{--height: {!! $chart->height() !!},--}}
                                                            {{--width: '{!! $chart->width() !!}',--}}
                                                            {{--toolbar: {--}}
                                                                {{--download: false,--}}
                                                                {{--show: true,--}}
                                                                {{--offsetX: 0,--}}
                                                                {{--offsetY: 0,--}}
                                                                {{--tools: {--}}
                                                                    {{--download: true,--}}
                                                                    {{--selection: true,--}}
                                                                    {{--zoom: false,--}}
                                                                    {{--zoomin: false,--}}
                                                                    {{--zoomout: false,--}}
                                                                    {{--pan: false,--}}
                                                                    {{--reset: false--}}
                                                                {{--},--}}
                                                                {{--export: {--}}
                                                                    {{--csv: {--}}
                                                                        {{--filename: 'data',--}}
                                                                        {{--columnDelimiter: ',',--}}
                                                                        {{--headerCategory: '',--}}
                                                                        {{--headerValue: 'value',--}}
                                                                        {{--dateFormatter(timestamp) {--}}
                                                                            {{--return new Date(timestamp).toDateString()--}}
                                                                        {{--}--}}
                                                                    {{--},--}}
                                                                    {{--svg: {--}}
                                                                        {{--filename: 'data',--}}
                                                                    {{--},--}}
                                                                    {{--png: {--}}
                                                                        {{--filename: 'data',--}}
                                                                    {{--}--}}
                                                                {{--}--}}
                                                            {{--},--}}
                                                        {{--},--}}
                                                        {{--plotOptions: {--}}
                                                            {{--bar: {!! $chart->horizontal() !!}--}}
                                                        {{--},--}}
                                                        {{--colors: {!! $chart->colors() !!},--}}
                                                        {{--series: {!! $chart->dataset() !!},--}}
                                                        {{--dataLabels: {!! $chart->dataLabels() !!},--}}
                                                        {{--@if($chart->labels())--}}
                                                        {{--labels: {!! json_encode($chart->labels(), true) !!},--}}
                                                        {{--@endif--}}
                                                        {{--title: {--}}
                                                            {{--text: "{!! $chart->title() !!}"--}}
                                                        {{--},--}}
                                                        {{--subtitle: {--}}
                                                            {{--text: '{!! $chart->subtitle() !!}',--}}
                                                            {{--align: '{!! $chart->subtitlePosition() !!}'--}}
                                                        {{--},--}}
                                                        {{--xaxis: {--}}
                                                            {{--categories: {!! $chart->xAxis() !!}--}}
                                                        {{--},--}}
                                                        {{--grid: {!! $chart->grid() !!},--}}
                                                        {{--markers: {!! $chart->markers() !!},--}}
                                                        {{--@if($chart->stroke())--}}
                                                        {{--stroke: {!! $chart->stroke() !!},--}}
                                                        {{--@endif--}}
                                                    {{--}--}}
                                                {{--var chartInstance = new ApexCharts(document.querySelector("#{!! $chart->id() !!}"), options);--}}
                                                {{--chartInstance.render();--}}
                                            {{--</script>--}}
                                        {{--@else--}}
                                            {{--<div class='no_content'><h3>Không có dữ liệu!</h3></div>--}}
                                        {{--@endif--}}

                                        {{--@if (!empty($reportsFilter))--}}
                                            {{--<hr>--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="table-responsive">--}}
                                                    {{--<table class="table">--}}
                                                        {{--<thead>--}}
                                                        {{--<tr>--}}
                                                            {{--<th>#</th>--}}
                                                            {{--<th>Tên camera</th>--}}
                                                            {{--<th>Số lượng người vào</th>--}}
                                                            {{--<th>Có khẩu trang</th>--}}
                                                            {{--<th>Không có khẩu trang</th>--}}
                                                            {{--<th>Giờ</th>--}}
                                                            {{--<th>Phút</th>--}}
                                                            {{--<th>Ngày</th>--}}
                                                            {{--<th>Tháng</th>--}}
                                                            {{--<th>Năm</th>--}}
                                                        {{--</tr>--}}
                                                        {{--</thead>--}}
                                                        {{--<tbody>--}}

                                                        {{--@foreach($reportsFilter as $key => $value)--}}
                                                            {{--<tr>--}}
                                                                {{--<th>{{$key+1}}</th>--}}
                                                                {{--<td>{{$value->camera->name}}</td>--}}
                                                                {{--<td>{{$value->people_entering}}</td>--}}
                                                                {{--<td>{{$value->people_have_mask}}</td>--}}
                                                                {{--<td>{{$value->people_no_mask}}</td>--}}
                                                                {{--<td>{{$value->hours}}</td>--}}
                                                                {{--<td>{{$value->minutes}}</td>--}}
                                                                {{--<td>{{$value->date}}</td>--}}
                                                                {{--<td>{{$value->month}}</td>--}}
                                                                {{--<td>{{$value->year}}</td>--}}
                                                            {{--</tr>--}}
                                                        {{--@endforeach--}}
                                                        {{--</tbody>--}}
                                                    {{--</table>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--@endif--}}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {

            var callAjax = true;

            var url_string = window.location.href;
            var url = new URL(url_string);
            var object_id = url.searchParams.get("object_id");
            var view_by = url.searchParams.get("view_by");
            var view_time = url.searchParams.get("view_time");
            var camera_id = url.searchParams.get("camera_id");

            // call ajax when click object from area manager objects
            if (callAjax == true) {
                var object_id = $('#objects').val();
                var camera_id = $('.cameras').val();

                var view_by = $('#view_by').val();
                var view_time = $('#view_time').val();

                // if (object_id == '') {
                //     $('.chart-analytics').html("<div class='no_content'><h3>Không có dữ liệu!</h3></div>");
                //     return;
                // }

                $.ajax({
                    url: "{{ route('viewChart') }}",
                    data: {
                        object_id: object_id,
                        camera_id: camera_id,
                        view_by: view_by,
                        view_time: view_time,
                    },
                    type: "GET",
                    success: function (res) {
                        if (res === 'FALSE') {
                            window.location = '/login';
                        } else {
                            $('.chart-analytics').html(res);
                        }
                    },
                    error: function () {
                    }
                });
            }

            var checkAll = JSON.parse("{{ json_encode($flagAllCamera) }}");


            if (object_id == '' &&  view_by == '1' && view_time == '') {
                $('#camera_select').empty();
            }

            if (checkAll === true) {
                $(".all").prop("checked", "checked");
            }

            // uncheck when clear item cameras
            $('#camera_select').change(function () {
                if ($(this).val() == null) {
                    $(".all").prop("checked", false);
                }
            });

            // check all when click checkbox
            $(".all").click(function () {
                if ($(".all").is(':checked')) {
                    $("#camera_select > option").prop("selected", "selected");
                    $("#camera_select").trigger("change");
                } else {
                    $("#camera_select > option").removeAttr("selected");
                    $("#camera_select").trigger("change");
                }
            });

            // placeholder for input time
            $("#view_time").inputmask("datetime", {alias: "datetime", inputFormat: "dd/mm/yyyy"});
            // $("#view_time").val(moment(new Date().toISOString().slice(0, 10)).format('DD/MM/YYYY'));

            // change format input time
            var viewBy = 0;
            $("#view_by").click(function () {
                viewBy = parseInt($(this).val());
                if (viewBy === 1) {
                    $("#view_time").inputmask("datetime", {alias: "datetime", inputFormat: "dd/mm/yyyy"});
                    $("#view_time").val('');
                }
                if (viewBy === 2) {
                    $("#view_time").inputmask("datetime", {alias: "datetime", inputFormat: "mm/yyyy"});
                    $("#view_time").val('');
                }
                if (viewBy === 3) {
                    $("#view_time").inputmask("datetime", {alias: "datetime", inputFormat: "yyyy"});
                    $("#view_time").val('');
                }
            });

            // get Cameras by Object ID
            $("#objects").change(function () {

                var objectId = $(this).val();

                $(".all").prop("checked", false);

                if (objectId == '') {
                    $("#camera_select").html('');
                    return;
                }

                var url = laroute.route('getCameras', {id: objectId});
                $.ajax({
                    url: url,
                    type: "GET",
                    success: function (res) {
                        $("#camera_select").html(res);
                    },
                    error: function () {
                    }
                });
            });

            // // case when click object from area manager objects
            // if ($('#objects').val()) {
            //     var objectId = $('#objects').val();
            //     var url = laroute.route('getCameras', {id: objectId})
            //     $.ajax({
            //         url: url,
            //         type: "GET",
            //         success: function(res){
            //             $("#camera_select").html(res);
            //         },
            //         error:function(){
            //         }
            //     });
            // }

            // Ajax view chart
            $('.btn-view-data').click(function () {
                $(this).attr("disabled", true);
                var object_id = $('#objects').val();
                var camera_id = $('.cameras').val();
                if (camera_id == null) {
                    camera_id = [];
                    camera_id[0] = '';
                }

                var view_by = $('#view_by').val();
                var view_time = $('#view_time').val();
                $.ajax({
                    url: "{{ route('viewChart') }}",
                    data: {
                        object_id: object_id,
                        camera_id: camera_id,
                        view_by: view_by,
                        view_time: view_time,
                    },
                    type: "GET",
                    success: function (res) {
                        if (res === 'FALSE') {
                            window.location = '/login';
                        } else {
                            $('.chart-analytics').html(res);
                            $('.btn-view-data').attr("disabled", false);
                        }
                    },
                    error: function () {
                    }
                });
            });
        });
    </script>
@endsection
