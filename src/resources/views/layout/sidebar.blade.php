<nav class="sidebar">
    <div class="sidebar-header">
        <a href="{{ url('/') }}" class="sidebar-brand">
            Face<span>Mask</span>
        </a>
        <div class="sidebar-toggler not-active">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <div class="sidebar-body">
        <ul class="nav">

            <li class="nav-item {{ active_class(['/']) }}">
                <a href="{{ url('/') }}" class="nav-link">
                    <i class="link-icon" data-feather="box"></i>
                    <span class="link-title">Trang chủ</span>
                </a>
            </li>

            <li class="nav-item {{ active_class(['analytics', '/analytics']) }}">
                <a class="nav-link" href="{{ route('dashboard.index') }}">
                    <i class="link-icon" data-feather="activity"></i>
                    <span class="link-title">Thống kê</span>
                </a>
            </li>


            @if (\Auth::user()->isAdmin())
                <li class="nav-item {{ active_class(['users', 'users/*']) }}">
                    <a class="nav-link" href="{{ route('users') }}">
                        <i class="link-icon" data-feather="users"></i>
                        <span class="link-title">Quản lý người dùng</span>
                    </a>
                </li>

                <li class="nav-item {{ active_class(['licences', 'licences/*']) }}">
                    <a class="nav-link" href="{{ route('licences.index') }}">
                        <i class="link-icon" data-feather="package"></i>
                        <span class="link-title">Quản lý giấy phép</span>
                    </a>
                </li>
            @endif

            <li class="nav-item {{ active_class(['objects', 'objects/*']) }}">
                <a class="nav-link" href="{{ route('objects') }}">
                    <i class="link-icon" data-feather="server"></i>
                    <span class="link-title">Quản lý khu vực</span>
                </a>
            </li>
            <li class="nav-item {{ active_class(['settings', 'settings/*']) }}">
                <a class="nav-link" href="" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="link-icon" data-feather="log-out"></i>
                    <span class="link-title">Đăng xuất</span>
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>
        </ul>
    </div>
</nav>
