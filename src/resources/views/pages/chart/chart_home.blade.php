<div class="row">
    <div class="col-lg-6 col-md-12">
        {!! $chartFirst->container() !!}
        <script>
            var options =
                {
                    chart: {
                        type: '{!! $chartFirst->type() !!}',
                        height: {!! $chartFirst->height() !!},
                        width: '{!! $chartFirst->width() !!}',
                        toolbar: {
                            download: false,
                            show: false,
                            offsetX: 0,
                            offsetY: 0,
                            tools: {
                                download: false,
                                selection: false,
                                zoom: false,
                                zoomin: false,
                                zoomout: false,
                                pan: false,
                                reset: false
                            },
                        },
                    },
                    plotOptions: {
                        bar: {!! $chartFirst->horizontal() !!}
                    },
                    colors: {!! $chartFirst->colors() !!},
                    series: {!! $chartFirst->dataset() !!},
                    dataLabels: {!! $chartFirst->dataLabels() !!},
                    @if($chartFirst->labels())
                    labels: {!! json_encode($chartFirst->labels(), true) !!},
                    @endif
                    title: {
                        text: "{!! $chartFirst->title() !!}"
                    },
                    subtitle: {
                        text: '{!! $chartFirst->subtitle() !!}',
                        align: '{!! $chartFirst->subtitlePosition() !!}'
                    },
                    xaxis: {
                        categories: {!! $chartFirst->xAxis() !!}
                    },
                    grid: {!! $chartFirst->grid() !!},
                    markers: {!! $chartFirst->markers() !!},
                    stroke: {
                        curve: 'smooth',
                        width: 2,
                    },
                }

            var chartFirst = new ApexCharts(document.querySelector("#{!! $chartFirst->id() !!}"), options);
            chartFirst.render();
        </script>
    </div>
    <div class="col-lg-6 col-md-12">
        {!! $chartSecond->container() !!}
        <script>
            var options =
                {
                    chart: {
                        type: '{!! $chartSecond->type() !!}',
                        height: {!! $chartSecond->height() !!},
                        width: '{!! $chartSecond->width() !!}',
                        toolbar: {
                            download: false,
                            show: false,
                            offsetX: 0,
                            offsetY: 0,
                            tools: {
                                download: false,
                                selection: false,
                                zoom: false,
                                zoomin: false,
                                zoomout: false,
                                pan: false,
                                reset: false
                            }
                        },
                    },
                    plotOptions: {
                        bar: {!! $chartSecond->horizontal() !!}
                    },
                    colors: {!! $chartSecond->colors() !!},
                    series: {!! $chartSecond->dataset() !!},
                    dataLabels: {!! $chartSecond->dataLabels() !!},
                    @if($chartSecond->labels())
                    labels: {!! json_encode($chartSecond->labels(), true) !!},
                    @endif
                    title: {
                        text: "{!! $chartSecond->title() !!}"
                    },
                    subtitle: {
                        text: '{!! $chartSecond->subtitle() !!}',
                        align: '{!! $chartSecond->subtitlePosition() !!}'
                    },
                    xaxis: {
                        categories: {!! $chartSecond->xAxis() !!}
                    },
                    grid: {!! $chartSecond->grid() !!},
                    markers: {!! $chartSecond->markers() !!},
                    stroke: {
                        curve: 'smooth',
                        width: 2,
                    },
                };

            var chartSecond = new ApexCharts(document.querySelector("#{!! $chartSecond->id() !!}"), options);
            chartSecond.render();
        </script>
    </div>
</div>
