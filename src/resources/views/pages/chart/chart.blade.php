{{--<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>--}}
<div class="row">
    <div class="col-md-3">
        {!! $pieChart->container() !!}
        <script>
            var options = {
                series: {!! $pieChart->dataset() !!},
                chart: {
                    width: 380,
                    type: 'pie',
                },
                colors: {!! $pieChart->colors() !!},
                labels: ['Đeo khẩu trang', 'Không đeo khẩu trang'],
                responsive: [{
                    breakpoint: 480,
                    options: {
                        chart: {
                            width: 200
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }],
                dataLabels: {
                    formatter(val, opts) {
                        const name = opts.w.globals.labels[opts.seriesIndex]
                        return [name, val.toFixed(1) + '%']
                    },
                },
                legend: {
                    show: false
                }
            };
            var pieChart = new ApexCharts(document.querySelector("#{!! $pieChart->id() !!}"), options);
            pieChart.render();
        </script>

        <div style="padding: 60px">
            <div class="apexcharts-legend-series" rel="1" style="margin: 2px 5px;">
                <span class="apexcharts-legend-marker" rel="1"
                      style="background: rgb(212, 225, 87) !important; color: rgb(212, 225, 87); height: 12px; width: 12px; left: 0px; top: 0px; border-width: 0px; border-color: rgb(255, 255, 255); border-radius: 12px;"></span>
                <span class="apexcharts-legend-text" rel="1"
                      i="0">Tổng người: {{explode(',', substr(rtrim($pieChart->dataset(), ']'),1))[1] + explode(',', substr(rtrim($pieChart->dataset(), ']'),1))[0]}}  </span>
            </div>
            <div class="apexcharts-legend-series" rel="2" style="margin: 2px 5px;">
                <span class="apexcharts-legend-marker" rel="2"
                      style="background: rgb(66, 165, 245) !important; color: rgb(66, 165, 245); height: 12px; width: 12px; left: 0px; top: 0px; border-width: 0px; border-color: rgb(255, 255, 255); border-radius: 12px;"></span>
                <span class="apexcharts-legend-text" rel="2" i="1">
                Đeo khẩu trang: {!! explode(',', substr(rtrim($pieChart->dataset(), ']'),1))[0] !!}</span>
            </div>
            <div class="apexcharts-legend-series" rel="3" style="margin: 2px 5px;">
                <span class="apexcharts-legend-marker" rel="3"
                      style="background: rgb(239, 83, 80) !important; color: rgb(239, 83, 80); height: 12px; width: 12px; left: 0px; top: 0px; border-width: 0px; border-color: rgb(255, 255, 255); border-radius: 12px;"></span>
                <span class="apexcharts-legend-text" rel="3"
                      i="2">Không đeo khẩu trang: {!! explode(',', substr(rtrim($pieChart->dataset(), ']'),1))[1] !!}</span>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        {!! $chart->container() !!}
        <script>
            var options =
                {
                    chart: {
                        type: '{!! $chart->type() !!}',
                        height: {!! $chart->height() !!},
                        width: '{!! $chart->width() !!}',
                        toolbar: {
                            download: false,
                            show: true,
                            offsetX: 0,
                            offsetY: 0,
                            tools: {
                                download: true,
                                selection: true,
                                zoom: false,
                                zoomin: false,
                                zoomout: false,
                                pan: false,
                                reset: false
                            },
                            export: {
                                csv: {
                                    filename: 'data',
                                    columnDelimiter: ',',
                                    headerCategory: '',
                                    headerValue: 'value',
                                    dateFormatter(timestamp) {
                                        return new Date(timestamp).toDateString()
                                    }
                                },
                                svg: {
                                    filename: 'data',
                                },
                                png: {
                                    filename: 'data',
                                }
                            }
                        },
                    },
                    plotOptions: {
                        bar: {!! $chart->horizontal() !!}
                    },
                    colors: {!! $chart->colors() !!},
                    series: {!! $chart->dataset() !!},
                    dataLabels: {!! $chart->dataLabels() !!},
                    @if($chart->labels())
                    labels: {!! json_encode($chart->labels(), true) !!},
                    @endif
                    title: {
                        text: "{!! $chart->title() !!}"
                    },
                    subtitle: {
                        text: '{!! $chart->subtitle() !!}',
                        align: '{!! $chart->subtitlePosition() !!}'
                    },
                    xaxis: {
                        categories: {!! $chart->xAxis() !!}
                    },
                    grid: {!! $chart->grid() !!},
                    markers: {!! $chart->markers() !!},
                    stroke: {
                        curve: 'smooth',
                        width: 2,
                    },
                }

            var chartInstance = new ApexCharts(document.querySelector("#{!! $chart->id() !!}"), options);
            chartInstance.render();
        </script>
    </div>
</div>
<hr>
<div class="row">
    <div class="table-responsive">
        <table class="table" id="tableAnalytic">
            <thead>
            <tr>
                <th>#</th>
                <th>Tên camera</th>
                <th>Thời gian</th>
                <th>Số lượng người vào</th>
                <th>Số lượng có khẩu trang</th>
                <th>Số lượng không có khẩu trang</th>
                <th>Tỉ lệ (Không đeo/Tổng)</th>
            </tr>
            </thead>
            <tbody>

            @if (!empty($cameras))

                @php $number = 1; @endphp

                @foreach($cameras as $key => $value)
                    @if (array_key_exists($value->id, $reportByCamera))

                        @foreach($reportByCamera[$value->id]['entering'] as $i => $item)

                            <tr style="color: {{
                                setColor(
                                array_key_exists($i, $reportByCamera[$value->id]['no_mask']) && $reportByCamera[$value->id]['no_mask'][$i] > 0
                                && array_key_exists($i, $reportByCamera[$value->id]['entering']) && $reportByCamera[$value->id]['entering'][$i] > 0
                                ? ($reportByCamera[$value->id]['no_mask'][$i] / $reportByCamera[$value->id]['entering'][$i] * 100)
                                : '0' )
                            }}; text-align: center">
                                <th>{{$number}}</th>
                                <td>{{$value->name}}</td>
                                <td>{{array_key_exists($i, $reportByCamera[$value->id]['collections']) ? $reportByCamera[$value->id]['collections'][$i] : ''}} {{$time}}</td>
                                {{--{{dd($reportByCamera[$value->id])}}--}}
                                <td>{{array_key_exists($i, $reportByCamera[$value->id]['entering']) ? $reportByCamera[$value->id]['entering'][$i] : '0'}}</td>
                                <td>{{array_key_exists($i, $reportByCamera[$value->id]['have_mask']) ? $reportByCamera[$value->id]['have_mask'][$i] : '0'}}</td>
                                <td>{{array_key_exists($i, $reportByCamera[$value->id]['no_mask']) ? $reportByCamera[$value->id]['no_mask'][$i] : '0'}}</td>
                                <td>
                                    {{array_key_exists($i, $reportByCamera[$value->id]['no_mask']) && $reportByCamera[$value->id]['no_mask'][$i] > 0
                                    && array_key_exists($i, $reportByCamera[$value->id]['entering']) && $reportByCamera[$value->id]['entering'][$i]> 0
                                        ? round(($reportByCamera[$value->id]['no_mask'][$i] / $reportByCamera[$value->id]['entering'][$i]) * 100, 2) . ' %'
                                        : '0 %'}}
                                </td>
                            </tr>

                            @php $number++; @endphp

                        @endforeach
                    @endif
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>

<script>
    $(document).ready( function () {
        $('#tableAnalytic').DataTable({
            // searching: false,
            info: true,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excel',
                    text: 'Xuất Excel',
                    className: 'btn btn-primary btn-icon-text',
                    title: 'Báo cáo thống kê',
                }
            ],
            "language": {
                info: "Hiển thị _START_ đến _END_ của _TOTAL_ bản ghi",
                search: "Tìm kiếm"
            }
        });
    } );
</script>

