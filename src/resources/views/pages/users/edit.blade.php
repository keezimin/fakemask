@extends('layout.master')

@section('content')
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/users">Người dùng</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tạo người dùng</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h6 class="card-title">Thông tin người quản lý</h6>
                    <form action="{{ route('users.update', ['id' => $user->id]) }}" method="POST">
                        @csrf

                        <div class="form-group">
                            <label>Tên</label>
                            <input type="text" class="form-control" placeholder="Enter Name"
                                   name="name"
                                   value="{{$user->name}}">

                            @error('name')
                                <label class="error mt-2 text-danger">
                                    {{ $message }}
                                </label>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" placeholder="Enter Email" disabled
                                   name="email"
                                   value="{{$user->email}}">

                            @error('email')
                                <label class="error mt-2 text-danger">
                                    {{ $message }}
                                </label>
                            @enderror
                        </div>
{{--                        <div class="form-group">--}}
{{--                            <label>Mật khẩu cũ</label>--}}
{{--                            <input type="password" class="form-control" placeholder="Nhập mật khẩu cũ"--}}
{{--                                   name="old_password"--}}
{{--                                   value="{{ old('old_password') }}">--}}

{{--                            @error('password')--}}
{{--                                <label class="error mt-2 text-danger">--}}
{{--                                    {{ $message }}--}}
{{--                                </label>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
                        <div class="form-group">
                            <label>Mật khẩu mới</label>
                            <input type="password" class="form-control" placeholder="Nhập mật khẩu mới"
                                   name="new_password"
                                   value="{{ old('new_password') }}">

                            @error('password_confirmation')
                            <label class="error mt-2 text-danger">
                                {{ $message }}
                            </label>
                            @enderror
                        </div>

                        <div class="form-group">
                            {{--<label>Quản lý:</label>--}}
                            {{--<select class="form-control" name="object">--}}
                                {{--@foreach($objects as $value)--}}
                                    {{--<option value="{{ $value->id }}">{{$value->name}}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}

                            <label>Quản lý khu vực:</label>
                            <select id="object_select" class="form-control select-nultiple w-100" name="objects[]" multiple="multiple" data-width="100%">
                                @foreach($objects as $value)
                                    <option value="{{ $value->id }}" @if (!empty($objectSelects) && in_array($value->id, $objectSelects)) selected @endif>{{$value->name}}</option>
                                @endforeach
                            </select>

                            @error('object')
                            <label class="error mt-2 text-danger">
                                {{ $message }}
                            </label>
                            @enderror
                        </div>

                        <button class="btn btn-success" type="submit">Lưu</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
