@extends('layout.master')

@section('content')

    <div class="row">
        <div class="col-12 col-xl-12 stretch-card">
            <div class="row flex-grow">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title d-flex justify-content-md-between align-items-center">
                                <div>Quản lý người dùng</div>
                                <a href="{{ route('users.create') }}" class="btn btn-success">Tạo mới</a>
                            </h6>

                            <div class="">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Tên</th>
                                        <th>Email</th>
                                        <th>Quản lý khu vực</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($items as $item)
                                        <tr>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->email }}</td>
                                            <td>
                                                @foreach($item->objectAppearance as $value)
                                                    <h1 class="badge badge-success"> {{ $value->name }}</h1>
                                                @endforeach
                                            </td>
                                            <td>
                                                <a href="{{ route('users.edit', ['id' => $item->id]) }}" class="btn btn-primary">Chỉnh sửa</a>
                                                <a href="{{ route('users.delete', ['id' => $item->id]) }}" class="btn btn-danger">Xoá</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        {{ $items->links('vendor.pagination.bootstrap-4') }}
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
