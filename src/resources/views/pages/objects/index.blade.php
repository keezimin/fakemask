@extends('layout.master')

@section('content')
    <div class="row">
        <div class="col-12 col-xl-12 stretch-card">
            <div class="row flex-grow">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title d-flex justify-content-md-between align-items-center">
                                <div>Quản lý khu vực</div>
                            </h6>

                            <div class="table-responsive" style="overflow-x:auto;">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Tên Khu Vực</th>
                                        <th>Số lượng camera</th>
                                        <th>Ngày tạo</th>
                                        <th>Thời gian đồng bộ gần nhất</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($items as $key => $item)
                                        <tr>
                                            <td>
                                                <a href="{{route('dashboard.index')}}?object_id={{$item->id}}&view_by=1&view_time={{array_key_exists($item->id, $latestSync) ? $latestSync[$item->id] : ''}}">
                                                    {{ $item->name }}
                                                </a>
                                            </td>
                                            <td>{{ $item->cameras->count()}}</td>
                                            <td>{{ $item->created_at->format('H:i d-m-Y') }}</td>
                                            <td>
                                                {{array_key_exists($item->id, $timeSync) ? $timeSync[$item->id] : ''}}
                                            </td>
                                            <td>
                                                <a class="btn btn-primary" href="{{route('cameraByObject', ['id' => $item->id])}}">Xem camera</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--    {{ $items->links('vendor.pagination.bootstrap-4') }}--}}
@endsection
