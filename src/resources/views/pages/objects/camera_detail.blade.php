@extends('layout.master')

@section('content')
    <div class="row">
        <div class="col-12 col-xl-12 stretch-card">
            <div class="row flex-grow">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title d-flex justify-content-md-between align-items-center">
                                <div>
                                    Khu vực: <a href="{{route('dashboard.index')}}?object_id={{$object->id}}&view_by=1&view_time={{\Carbon\Carbon::now()->format('d/m/Y')}}">
                                        {{ $object->name }}
                                    </a>
                                </div>
                            </h6>

                            @if (count($cameras))
                            <div class="table-responsive" style="overflow-x:auto;">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Tên camera</th>
                                        <th>Ngày tạo</th>
                                        <th>Thời gian đồng bộ gần nhất</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($cameras as $key => $item)
                                        <tr>
                                            <td>
                                                <a href="{{route('dashboard.index')}}?object_id={{$object->id}}&camera_id={{$item->id}}&view_by=1&view_time={{array_key_exists($item->id, $latestSync) ? $latestSync[$item->id] : ''}}">
                                                    {{ $item->name }}
                                                </a>
                                            </td>
                                            <td>{{ $item->created_at->format('H:i d-m-Y') }}</td>
                                            <td>
                                                @foreach ($data as $value)
                                                    @if (!empty($value) && $item->id == $value->camera_id)
                                                        {{$value->hours}}:{{$value->minutes}} {{$value->date}}/{{$value->month}}/{{$value->year}}
                                                    @endif
                                                @endforeach
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @else
                                <div class='no_content'><h3>Không có dữ liệu!</h3></div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--    {{ $items->links('vendor.pagination.bootstrap-4') }}--}}
@endsection
