@extends('layout.master')

@section('content')

    <div class="row">
        <div class="col-12 col-xl-12 stretch-card">
            <div class="row flex-grow">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title d-flex justify-content-md-between align-items-center">
                                <div>Quản lý giấy phép</div>
                                <a href="{{ route('licences.create') }}" class="btn btn-success">Tạo mới</a>
                            </h6>

                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Giấy phép</th>
                                        <th>Khu vực</th>
                                        <th>Trạng Thái</th>
                                        <th>Ngày tạo</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($items as $key => $item)
                                        <tr>
                                            <td>{{ $item->licence }}</td>
                                            <td>
                                                @if ($item->objectAppearance)
                                                    <h5>
                                                        <a href="{{route('dashboard.index')}}?object_id={{$item->objectAppearance->id}}&view_by=1&view_time={{\Carbon\Carbon::now()->format('d/m/Y')}}">{{ $item->objectAppearance->name }}</a>
                                                    </h5>
                                                @endif
                                            </td>
                                            <td>
                                                @if ( $item->status === Modules\Licence\Models\Licence::INACTIVE )
                                                    <span class="badge badge-secondary">Chưa sử dụng</span>
                                                @else
                                                    <span class="badge badge-success">Đã sử dụng</span>
                                                @endif
                                            </td>
                                            <td>{{ $item->created_at->format('H:i d-m-Y') }}</td>
                                            <td>
                                                @if ( $item->status === Modules\Licence\Models\Licence::INACTIVE )
                                                    <input class="btn btn-danger" type="submit" value="Xóa" onclick="showSwal({{$item->id}})"/>
                                                    {{--<form action="{{ route('licences.destroy', ['licence' => $item->id]) }}" method="post">--}}
                                                        {{--<input class="btn btn-danger" type="submit" value="Xóa" onclick="showSwal({{$item->id}})"/>--}}
                                                        {{--@method('delete')--}}
                                                        {{--@csrf--}}
                                                    {{--</form>--}}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        {{ $items->links('vendor.pagination.bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
