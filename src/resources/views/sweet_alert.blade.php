<script>
    function showSwal(licenceId) {

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false,
        })

        swalWithBootstrapButtons.fire({
            title: 'Bạn chắc chắn muốn xóa?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'ml-2',
            confirmButtonText: 'Có',
            cancelButtonText: 'Không!',
            reverseButtons: true
        }).then((result) => {

            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "{{ route('licences.deleteLicenceAjax') }}",
                    type: 'post',
                    data: {
                        id: licenceId
                    },
                    dataType: "html",
                    success: function (data) {
                        if (data == 1) {
                            swalWithBootstrapButtons.fire(
                                'Giấy phép đã được xóa',
                                '',
                                'success'
                            ).then((result) => {
                                if (result.value) {
                                    location.reload();
                                }
                            })
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swalWithBootstrapButtons.fire(
                            'Lỗi server',
                            '',
                            'error'
                        )
                    }
                });


                // swalWithBootstrapButtons.fire(
                //     'Giấy phép đã được xóa',
                //     '',
                //     'success'
                // )

            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                    'Hủy xóa giấy phép',
                    '',
                    'error'
                )
            }
        })
    }

</script>
