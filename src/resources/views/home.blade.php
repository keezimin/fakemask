@extends('layout.master')

@section('content')
    <div class="row">
        <div class="col-12 col-xl-12 stretch-card">
            <div class="row flex-grow">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="custom-col-20">
                                    <div class="form-group">
                                        <label for="view_by">Xem theo</label>
                                        <select class="form-control" id="view_by">
                                            <option value='2' @if (request()->view_by == 2) selected @endif>Theo tháng
                                            </option>
                                            <option value='3' @if (request()->view_by == 3) selected @endif>Theo năm
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="custom-col-20">
                                    <div class="form-group">
                                        <label for="datePickerFrom">Chọn thời gian</label>
                                        <div class="input-group">
                                            <input id="view_time" class="form-control" value="" inputmode="numeric">
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-col-20" style="margin-top: 29px;">
                                    <div class="form-group" style="width: 100%">
                                        <button type="button"
                                                class="btn-view-data btn btn-success btn-icon-text mb-2 mb-md-0"
                                                style="height: 36px; width: 100%">
                                            Xem
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12" style="padding: 10px 0 10px 0">
                                    <div class="chart-analytics-home">
                                        {{--@if(!empty($chart))--}}
                                            {{--{!! $chart->container() !!}--}}
                                            {{--<script>--}}
                                                {{--var options =--}}
                                                    {{--{--}}
                                                        {{--chart: {--}}
                                                            {{--type: '{!! $chart->type() !!}',--}}
                                                            {{--height: {!! $chart->height() !!},--}}
                                                            {{--width: '{!! $chart->width() !!}',--}}
                                                            {{--toolbar: {--}}
                                                                {{--download: false,--}}
                                                                {{--show: true,--}}
                                                                {{--offsetX: 0,--}}
                                                                {{--offsetY: 0,--}}
                                                                {{--tools: {--}}
                                                                    {{--download: true,--}}
                                                                    {{--selection: true,--}}
                                                                    {{--zoom: false,--}}
                                                                    {{--zoomin: false,--}}
                                                                    {{--zoomout: false,--}}
                                                                    {{--pan: false,--}}
                                                                    {{--reset: false--}}
                                                                {{--},--}}
                                                                {{--export: {--}}
                                                                    {{--csv: {--}}
                                                                        {{--filename: 'data',--}}
                                                                        {{--columnDelimiter: ',',--}}
                                                                        {{--headerCategory: '',--}}
                                                                        {{--headerValue: 'value',--}}
                                                                        {{--dateFormatter(timestamp) {--}}
                                                                            {{--return new Date(timestamp).toDateString()--}}
                                                                        {{--}--}}
                                                                    {{--},--}}
                                                                    {{--svg: {--}}
                                                                        {{--filename: 'data',--}}
                                                                    {{--},--}}
                                                                    {{--png: {--}}
                                                                        {{--filename: 'data',--}}
                                                                    {{--}--}}
                                                                {{--}--}}
                                                            {{--},--}}
                                                        {{--},--}}
                                                        {{--plotOptions: {--}}
                                                            {{--bar: {!! $chart->horizontal() !!}--}}
                                                        {{--},--}}
                                                        {{--colors: {!! $chart->colors() !!},--}}
                                                        {{--series: {!! $chart->dataset() !!},--}}
                                                        {{--dataLabels: {!! $chart->dataLabels() !!},--}}
                                                        {{--@if($chart->labels())--}}
                                                        {{--labels: {!! json_encode($chart->labels(), true) !!},--}}
                                                        {{--@endif--}}
                                                        {{--title: {--}}
                                                            {{--text: "{!! $chart->title() !!}"--}}
                                                        {{--},--}}
                                                        {{--subtitle: {--}}
                                                            {{--text: '{!! $chart->subtitle() !!}',--}}
                                                            {{--align: '{!! $chart->subtitlePosition() !!}'--}}
                                                        {{--},--}}
                                                        {{--xaxis: {--}}
                                                            {{--categories: {!! $chart->xAxis() !!}--}}
                                                        {{--},--}}
                                                        {{--grid: {!! $chart->grid() !!},--}}
                                                        {{--markers: {!! $chart->markers() !!},--}}
                                                        {{--@if($chart->stroke())--}}
                                                        {{--stroke: {!! $chart->stroke() !!},--}}
                                                        {{--@endif--}}
                                                    {{--}--}}
                                                {{--var chartInstance = new ApexCharts(document.querySelector("#{!! $chart->id() !!}"), options);--}}
                                                {{--chartInstance.render();--}}
                                            {{--</script>--}}
                                        {{--@else--}}
                                            {{--<div class='no_content'><h3>Không có dữ liệu!</h3></div>--}}
                                        {{--@endif--}}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {

            var callAjax = true;

            var url_string = window.location.href;
            var url = new URL(url_string);
            var view_by = url.searchParams.get("view_by");
            var view_time = url.searchParams.get("view_time");

            // placeholder for input time
            $("#view_time").inputmask("datetime", {alias: "datetime", inputFormat: "mm/yyyy"});
            $("#view_time").val(moment(new Date().toISOString().slice(0, 10)).format('MM/YYYY'));

            // change format input time
            var viewBy = 0;
            $("#view_by").click(function () {
                viewBy = parseInt($(this).val());
                if (viewBy === 2) {
                    $("#view_time").inputmask("datetime", {alias: "datetime", inputFormat: "mm/yyyy"});
                    $("#view_time").val('');
                }
                if (viewBy === 3) {
                    $("#view_time").inputmask("datetime", {alias: "datetime", inputFormat: "yyyy"});
                    $("#view_time").val('');
                }
            });

            // call ajax when click object from area manager objects
            if (callAjax == true) {

                var view_by = $('#view_by').val();
                var view_time = $('#view_time').val();
                $.ajax({
                    url: "{{ route('viewChartHome') }}",
                    data: {
                        view_by: view_by,
                        view_time: view_time,
                    },
                    type: "GET",
                    success: function (res) {
                        if (res === 'FALSE') {
                            window.location = '/login';
                        } else {
                            $('.chart-analytics-home').html(res);
                        }
                    },
                    error: function () {
                    }
                });
            }

            // Ajax view chart
            $('.btn-view-data').click(function () {

                var view_by = $('#view_by').val();
                var view_time = $('#view_time').val();
                $.ajax({
                    url: "{{ route('viewChartHome') }}",
                    data: {
                        view_by: view_by,
                        view_time: view_time,
                    },
                    type: "GET",
                    success: function (res) {
                        if (res === 'FALSE') {
                            window.location = '/login';
                        } else {
                            $('.chart-analytics-home').html(res);
                        }
                    },
                    error: function () {
                    }
                });
            });

        });
    </script>
@endsection
