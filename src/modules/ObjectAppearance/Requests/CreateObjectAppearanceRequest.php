<?php

namespace Modules\ObjectAppearance\Requests;

use Infrastructure\Requests\BaseCRUDRequest;
use Modules\Licence\Models\Licence;
use Modules\ObjectAppearance\Models\ObjectAppearance;

class CreateObjectAppearanceRequest extends BaseCRUDRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'object_name' => [
                'required',
                'string',
                function ($attribute, $value, $fail) {
                    $exist = ObjectAppearance::where('name', $value)->first();
                    
                    if (!$exist) {
                        return true;
                    }
                    return $fail("Tên đã tồn tại ");
                },
            ],
            'licence'     => [
                'required',
                'string',
                function ($attribute, $value, $fail) {
                    $exist = Licence::where(['status' => Licence::INACTIVE, 'object_appearance_id' => null])->pluck('licence')->toArray();
                    
                    if (in_array($value, $exist)) {
                        return true;
                    }
                    return $fail("Giấy phép đã được sử dụng hoặc không có sẳn");
                },
            ]
        ];
    }
}
