<?php

namespace Modules\ObjectAppearance\Requests;

use Infrastructure\Requests\BaseCRUDRequest;

class CreateCameraRequest extends BaseCRUDRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'object_appearance_id' => 'required|exists:object_appearances,id',
            'name'                 => 'required',
        ];
    }
}
