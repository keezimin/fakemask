<?php

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::get('/', 'ObjectAppearanceController@index')->name('objects');
    Route::get('/{id}/cameras', 'ObjectAppearanceController@cameraByObject')->name('cameraByObject');
});
