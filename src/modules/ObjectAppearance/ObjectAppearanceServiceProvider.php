<?php

namespace Modules\ObjectAppearance;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Route;

class ObjectAppearanceServiceProvider extends RouteServiceProvider
{
    protected $namespace = 'Modules\ObjectAppearance\Controllers';
    
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
    
    public function map()
    {
        $this->mapWebRoutes();
        
        $this->mapApiRoutes();
    }
    
    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::prefix('objects')
            ->namespace($this->namespace)
            ->group(base_path('modules/ObjectAppearance/routes.php'));
    }
    
    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api/objects')
            ->namespace($this->namespace)
            ->group(base_path('modules/ObjectAppearance/routes.api.php'));
    }
}
