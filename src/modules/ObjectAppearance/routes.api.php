<?php


Route::group(['middleware' => 'token'], function () {
    // Sync data from client
    Route::post('/sync', 'ObjectAppearanceController@sync');
    
    // Get latest resutl data by object
    Route::post('/get_latest_result_sync', 'ObjectAppearanceController@getLatestResultSync');
});

Route::post('/store', 'ObjectAppearanceController@store');
