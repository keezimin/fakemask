<?php

namespace Modules\ObjectAppearance\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Modules\Camera\Models\Camera;
use Modules\Licence\Models\Licence;
use Modules\ObjectAppearance\Models\ObjectAppearance;
use Modules\ObjectAppearance\Models\Report;
use Modules\ObjectAppearance\Requests\CreateObjectAppearanceRequest;
use Illuminate\Support\Facades\Auth;

class ObjectAppearanceController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data = [];
        $latestSync = [];
        
        $user = Auth::id();
        if (Auth::user()->isAdmin()) {
        
            $items = ObjectAppearance::with(['cameras' => function ($query) {
                $query->with('reports');
            }])->get();
        
            foreach ($items as $value) {
                $data[] = DB::table('object_appearances')
                    ->selectRaw('object_appearances.id as object_id, reports.* ')
                    ->join('cameras', 'object_appearances.id', '=', 'cameras.object_appearance_id')
                    ->join('reports', 'reports.camera_id', '=', 'cameras.id')
                    ->where('object_appearances.id', $value->id)
                    ->orderByDesc('reports.id')
                    ->limit(1)
                    ->first();
            }
        
        } else {
            $user = Auth::user();
            $items = $user->objectAppearance()->with(['cameras' => function ($query) {
                $query->with('reports');
            }])->get();
    
            foreach ($items as $value) {
                $data[] = DB::table('object_appearances')
                    ->selectRaw('object_appearances.id as object_id, reports.* ')
                    ->join('cameras', 'object_appearances.id', '=', 'cameras.object_appearance_id')
                    ->join('reports', 'reports.camera_id', '=', 'cameras.id')
                    ->where('object_appearances.id', $value->id)
                    ->orderByDesc('reports.id')
                    ->limit(1)
                    ->first();
            }
        }

        foreach ($items as $key => $item){
            foreach ($data as $value) {
                if (!empty($value) && $item->id == $value->object_id) {
                    $latestSync[$item->id] = Carbon::parse($value->month .'/'. $value->date .'/'. $value->year)->format('d/m/Y');
                    $timeSync[$item->id] = $value->hours . ':' . $value->minutes . ' ' . Carbon::parse($value->month .'/'. $value->date .'/'. $value->year)->format('d/m/Y');
                }
            }
        }

        return view('pages.objects.index', compact('items', 'data', 'latestSync', 'timeSync'));
    }
    
    /**
     * @param CreateObjectAppearanceRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateObjectAppearanceRequest $request)
    {
        $object = ObjectAppearance::create(['name' => $request->object_name]);
        $token = md5('face_mask' . Carbon::now()->toString() . time());
        $licenceUpdate = Licence::where('licence', $request->licence)->update(
            [
                'status'               => Licence::ACTIVE,
                'object_appearance_id' => $object->id,
                'token'                => $token
            ]
        );
        
        return $this->success($object, trans('messages.common.createSuccess'), ['isContainByDataString' => true]);
    }
    
    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function sync(Request $request)
    {
        $cameraResult = [];
        
        $objectExist = ObjectAppearance::findOrFail($request->object_id);
        $report = [];
        if ($objectExist) {
            $data = $request->data;
            foreach ($data as $value) {
                $camera = Camera::findOrFail($value['camera_id']);
                
                if ($camera) {
                    foreach ($value['data'] as $item) {
                        $item = explode(',', $item);
                        $data = [
                            'people_entering'  => $item[0],
                            'people_have_mask' => $item[1],
                            'people_no_mask'   => $item[2],
                            'minutes'          => $item[3],
                            'hours'            => $item[4],
                            'date'             => $item[5],
                            'month'            => $item[6],
                            'year'             => $item[7],
                            'camera_id'        => $value['camera_id'],
                        ];
                        
                        $report = Report::create($data);
                    }
                }
                
                $cameraResult[] = Report::select('id', 'camera_id', 'minutes', 'hours', 'date', 'month', 'year')
                    ->where('camera_id', $value['camera_id'])->latest('id')->first();
            }
        }
        
        return $this->success(collect($cameraResult), trans('messages.common.createSuccess'), ['isContainByDataString' => true]);
    }
    
    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function getLatestResultSync(Request $request)
    {
        $report = [];
        $object = ObjectAppearance::findOrFail($request->object_id);
        $cameras = $object->cameras;
        
        foreach ($cameras as $value) {
            $latestUpdated = Report::select('id', 'camera_id', 'minutes', 'hours', 'date', 'month', 'year')
                ->where('camera_id', $value->id)->latest('id')->first();

            if ($latestUpdated) {
                $report[] = $latestUpdated;
            } else {
                $report[] = [
                    "camera_id" => $value->id,
                    "minutes" => '',
                    "hours" => '',
                    "date" => '',
                    "month" => '',
                    "year" => ''
                ];
            }
           
        }

        return $this->success(collect($report), trans('messages.common.createSuccess'), ['isContainByDataString' => true]);
    }
    
    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cameraByObject(Request $request, $id)
    {
        $object = ObjectAppearance::findOrFail($id);
        $cameras = $object->cameras;
        $data = [];
        foreach ($cameras as $value) {
            $data[] = DB::table('object_appearances')
                ->selectRaw('object_appearances.id as object_id, reports.* ')
                ->join('cameras', 'object_appearances.id', '=', 'cameras.object_appearance_id')
                ->join('reports', 'reports.camera_id', '=', 'cameras.id')
                ->where('object_appearances.id', $id)
                ->where('cameras.id', $value->id)
                ->orderByDesc('reports.id')
                ->limit(1)
                ->first();
        }
    
        $latestSync = [];
        
        foreach ($cameras as $key => $item){
            foreach ($data as $value) {
                if (isset($value) && $item->id == $value->camera_id) {
                    $latestSync[$value->camera_id] = Carbon::parse($value->month .'/'. $value->date .'/'. $value->year)->format('d/m/Y');
                }
            }
        }

        return view('pages.objects.camera_detail', compact('cameras', 'data', 'object', 'latestSync'));
    }
}
