<?php

namespace Modules\ObjectAppearance\Repositories;

use Infrastructure\BaseRepository;
use Modules\ObjectAppearance\Models\ObjectAppearance;

class ObjectAppearanceRepository extends BaseRepository
{
    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        parent::__construct(ObjectAppearance::class);
    }
}
