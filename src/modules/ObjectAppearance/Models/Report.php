<?php

namespace Modules\ObjectAppearance\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Camera\Models\Camera;

class Report extends Model
{
    CONST VIEW_DATE = 1;
    CONST VIEW_MONTH = 2;
    CONST VIEW_YEAR = 3;
    
    protected $fillable = [
        'camera_id', 'people_entering', 'people_have_mask', 'people_no_mask', 'minutes', 'hours', 'date', 'month', 'year'
    ];
    
    public function camera()
    {
        return $this->belongsTo(Camera::class);
    }
}
