<?php

namespace Modules\ObjectAppearance\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Camera\Models\Camera;
use Modules\Licence\Models\Licence;
use Modules\User\Models\User;

class ObjectAppearance extends Model
{
    protected $fillable = [
        'name',
    ];
    
    protected $appends = [
        'token'
    ];
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cameras()
    {
        return $this->hasMany(Camera::class);
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function license()
    {
        return $this->hasOne(Licence::class);
    }
    
    /**
     * @return mixed
     */
    public function getTokenAttribute()
    {
        return $this->license->token;
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_object_appearances', 'object_appearance_id', 'user_id');
    }
}
