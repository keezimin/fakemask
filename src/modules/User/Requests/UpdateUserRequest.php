<?php

namespace Modules\User\Requests;

use Infrastructure\Requests\BaseCRUDRequest;

class UpdateUserRequest extends BaseCRUDRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'old_password' => 'nullable|string',
            'new_password' => 'nullable|string',
        ];
    }
}
