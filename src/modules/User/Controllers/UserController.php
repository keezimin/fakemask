<?php

namespace Modules\User\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Modules\ObjectAppearance\Models\ObjectAppearance;
use Modules\User\Models\User;
use Modules\User\Requests\CreateUserRequest;
use Modules\User\Requests\UpdateUserRequest;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $items = User::where('id', '!=', Auth::user()->id)->with('objectAppearance')->orderByDesc('id')->paginate(5);
        return view('pages.users.index', compact('items'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $objects = ObjectAppearance::all();

        return view('pages.users.create', compact('objects'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $objectSelects = [];
        foreach ($user->objectAppearance as $value) {
            $objectSelects[] = $value->pivot->object_appearance_id;
        }

        $objects = ObjectAppearance::all();

        return view('pages.users.edit', compact('objects', 'user', 'objectSelects'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $data = $request->all();
        $user = User::findOrFail($id);
//        $current_password = $user->password;
//        if(Hash::check($data['old_password'], $current_password)){
//            $user->update(['password' => Hash::make($data['new_password'])]);
//        }

        if (!empty($data['new_password'])) {
            $user->update(['password' => Hash::make($data['new_password'])]);
        }
        $user->objectAppearance()->sync(!empty($data['objects']) ? $data['objects'] : []);

        return redirect()->route('users');
    }

    /**
     * @param \Modules\User\Requests\CreateUserRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function store(CreateUserRequest $request)
    {
        $data = $request->all();
        $data['role'] = 2;
        $data['password'] = Hash::make($request->password);
        $user = User::create($data);
        $user->objectAppearance()->sync(!empty($data['objects']) ? $data['objects'] : []);

        return redirect()->route('users');
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function delete(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->back();
    }
}
