<?php

namespace Modules\User\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\ObjectAppearance\Models\ObjectAppearance;
use phpDocumentor\Reflection\Types\Object_;

class User extends Authenticatable
{
    use Notifiable;

    const ROLE_ADMIN = 1;
    const ROLE_USER = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role', 'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

//    /**
//     * @return \Illuminate\Database\Eloquent\Relations\HasOne
//     */
//    public function objectAppearance()
//    {
//        return $this->hasOne(ObjectAppearance::class);
//    }
//
    public function objectAppearance() {
        return $this->belongsToMany(ObjectAppearance::class , 'user_object_appearances', 'user_id', 'object_appearance_id');
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->role === self::ROLE_ADMIN;
    }
}
