<?php

Route::group(['middleware' => ['web','auth']], function () {
    Route::get('/', 'UserController@index')->name('users');

    Route::get('/create', 'UserController@create')->name('users.create');
    Route::post('/create', 'UserController@store')->name('users.store');
    Route::get('/edit/{id}', 'UserController@edit')->name('users.edit');
    Route::post('/edit/{id}', 'UserController@update')->name('users.update');
    Route::get('/delete/{id}', 'UserController@delete')->name('users.delete');
});
