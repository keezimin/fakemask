<?php

namespace Modules\Camera\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\ObjectAppearance\Models\ObjectAppearance;
use Modules\ObjectAppearance\Models\Report;

class Camera extends Model
{
    protected $fillable = [
        'object_appearance_id', 'name'
    ];
    
    public function reports()
    {
        return $this->hasMany(Report::class);
    }
    
    public function object()
    {
        return $this->belongsTo(ObjectAppearance::class);
    }
}
