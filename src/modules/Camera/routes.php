<?php


Route::group(['middleware' => 'token'], function () {
    Route::post('/', 'CameraController@store');
    Route::patch('/{id}', 'CameraController@update');
    Route::delete('/{id}', 'CameraController@delete');
});
