<?php

namespace Modules\Camera\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Camera\Models\Camera;
use Modules\Camera\Requests\CreateCameraRequest;
use Modules\ObjectAppearance\Models\ObjectAppearance;
use Modules\ObjectAppearance\Models\Report;

class CameraController extends Controller
{
    /**
     * @param CreateCameraRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCameraRequest $request)
    {
        $camera = Camera::create($request->all());
        
        return $this->success($camera, trans('messages.common.createSuccess'), ['isContainByDataString' => true]);
    }
    
    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $camera = Camera::findOrFail($id);
        $camera->update($request->all());
        
        return $this->success($camera, trans('messages.common.modifySuccess'), ['isContainByDataString' => true]);
    }
    
    /**
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $camera = Camera::findOrFail($id);
        $camera->delete();
        
        return $this->success([], trans('messages.common.deleteSuccess'), ['isContainByDataString' => true]);
    }
    
    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function sync(Request $request)
    {
        $cameraResult = [];
        
        $objectExist = ObjectAppearance::findOrFail($request->object_id);
        $report = [];
        if ($objectExist) {
            $data = $request->data;
            foreach ($data as $value) {
                $camera = Camera::findOrFail($value['camera_id']);
                
                if ($camera) {
                    foreach ($value['data'] as $item) {
                        $item = explode(',', $item);
                        $data = [
                            'people_entering'  => $item[0],
                            'people_have_mask' => $item[1],
                            'people_no_mask'   => $item[2],
                            'minutes'          => $item[3],
                            'hours'            => $item[4],
                            'date'             => $item[5],
                            'month'            => $item[6],
                            'year'             => $item[7],
                            'camera_id'        => $value['camera_id'],
                        ];
                        
                        $report = Report::create($data);
                    }
                }
                
                $cameraResult[] = Report::select('id', 'camera_id', 'minutes', 'hours', 'date', 'month', 'year')
                    ->where('camera_id', $value['camera_id'])->latest('id')->first();
            }
        }
        
        return $this->success(collect($cameraResult), trans('messages.common.createSuccess'), ['isContainByDataString' => true]);
    }
    
}
