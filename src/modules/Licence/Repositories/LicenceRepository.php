<?php

namespace Modules\Licence\Repositories;

use Infrastructure\BaseRepository;
use Modules\Licence\Models\Licence;

class LicenceRepository extends BaseRepository
{
    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        parent::__construct(Licence::class);
    }
}
