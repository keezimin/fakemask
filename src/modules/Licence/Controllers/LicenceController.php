<?php

namespace Modules\Licence\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Licence\Models\Licence;

class LicenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Licence::paginate(10);
        return view('pages.licences.index', compact('items'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $count = env('NUMBER_GENERATE_LICENCE', 5);
        
        for ($x = 1; $x <= $count; $x++) {
            $license = generate_license();
            Licence::create(['licence' => $license, 'status' => Licence::INACTIVE]);
        }
        
        return redirect()->back();
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $licence = Licence::findOrFail($id);
        $licence->delete();
        return redirect()->back();
    }
    
    /**
     * @param Request $request
     * @return bool
     */
    public function deleteLicenceAjax(Request $request)
    {
        $id = $request->id;
        $licence = Licence::findOrFail($id);
        $licence->delete();
        return true;
    }
}
