<?php

namespace Modules\Licence;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Route;

class LicenceServiceProvider extends RouteServiceProvider
{
    protected $namespace = 'Modules\Licence\Controllers';
    
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
    
    public function map()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
    }
    
    
    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api/licences')
            ->namespace($this->namespace)
            ->middleware('token')
            ->group(base_path('modules/Licence/routes.api.php'));
    }
    
    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::prefix('')
            ->namespace($this->namespace)
            ->group(base_path('modules/Licence/routes.php'));
    }
}
