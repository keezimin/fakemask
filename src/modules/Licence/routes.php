<?php


Route::group(['middleware' => ['web', 'auth']], function () {
    Route::resource('/licences', 'LicenceController');
    Route::post('/licences', 'LicenceController@deleteLicenceAjax')->name('licences.deleteLicenceAjax');
});
