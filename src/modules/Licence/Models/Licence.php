<?php

namespace Modules\Licence\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\ObjectAppearance\Models\ObjectAppearance;

class Licence extends Model
{
    CONST ACTIVE = 'active';
    CONST INACTIVE = 'inactive';
    
    protected $fillable = [
        'licence', 'status', 'token'
    ];
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function objectAppearance()
    {
        return $this->belongsTo(ObjectAppearance::class);
    }
}
