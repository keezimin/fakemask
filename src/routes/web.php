<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Application layer


Route::group(['namespace' => 'App\Http\Controllers'], function () {
    Auth::routes(['register' => false]);
});

Route::group(['middleware' => ['auth'], 'namespace' => 'App\Http\Controllers'], function () {
    Route::get('/analytics', 'DashboardController@index')->name('dashboard.index');
    Route::get('/', 'DashboardController@home')->name('home');
    // Ajax get camera
    Route::get('/get_cameras/{id}', 'DashboardController@getCamera')->name('getCameras');
    
});

Route::get('/view_chart', 'App\Http\Controllers\DashboardController@viewChart')->name('viewChart');
Route::get('/view_chart/home', 'App\Http\Controllers\DashboardController@viewChartHome')->name('viewChartHome');
