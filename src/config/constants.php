<?php

return [
    'HTTP_STATUS_CODE' => [
        'NOT_FOUND'            => 404,
        'BAD_REQUEST'          => 400,
        'SERVER_ERROR'         => 500,
        'METHOD_NOT_ALLOWED'   => 405,
        'UNAUTHORIZED'         => 401,
        'PERMISSION_DENIED'    => 403,
        'UNPROCESSABLE_ENTITY' => 422,
        'NOT_ACCEPTABLE'       => 406,
        'SUCCESS'              => 200,
    ],
    'TOKEN' => 'd41d8cd98f00b204e9800998ecf8427e',
    'DEFAULT_VALUE_DATE_CHART' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    'DEFAULT_VALUE_MONTH_CHART' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
];
