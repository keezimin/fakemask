<?php

function active_class($path, $active = 'active') {
  return call_user_func_array('Request::is', (array)$path) ? $active : '';
}

function is_active_route($path) {
  return call_user_func_array('Request::is', (array)$path) ? 'true' : 'false';
}

function show_class($path) {
  return call_user_func_array('Request::is', (array)$path) ? 'show' : '';
}

function setColor ($percent){
    $color = '';

    if ($percent >= 50 && $percent < 70) {
        $color = 'blue';
    }

    if ($percent >= 70 && $percent < 90) {
        $color = '#F9A825';
    }
    
    if ($percent >= 90) {
        $color = 'red';
    }

    return $color;
}

function generate_license($suffix = null) {
    // Default tokens contain no "ambiguous" characters: 1,i,0,o
    if(isset($suffix)){
        // Fewer segments if appending suffix
        $num_segments = 3;
        $segment_chars = 6;
    }else{
        $num_segments = 5;
        $segment_chars = 10;
    }
    $tokens = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789' . implode( '-',str_split( strtoupper(time()), 12)) . uniqid();
    $license_string = '';

    // Build Default License String
    for ($i = 0; $i < $num_segments; $i++) {
        $segment = '';
        for ($j = 0; $j < $segment_chars; $j++) {
            $segment .= $tokens[rand(0, strlen($tokens)-1)];
        }
        $license_string .= $segment;
        if ($i < ($num_segments - 1)) {
            $license_string .= '-' ;
        }
    }
 
    // If provided, convert Suffix
    if(isset($suffix)){
        if(is_numeric($suffix)) {   // Userid provided
            $license_string .= '-'.strtoupper(base_convert($suffix,10,36)) . '-' . implode( '-',str_split( strtoupper(time()), 12));
        }else{
            $long = sprintf("%u\n", ip2long($suffix),true);
            if($suffix === long2ip($long) ) {
                $license_string .= '-'.strtoupper(base_convert($long,10,36)) . '-' . implode( '-',str_split( strtoupper(time()), 12));
            }else{
                $license_string .= '-'.strtoupper(str_ireplace(' ','-',$suffix)) . '-' . implode( '-',str_split( strtoupper(time()), 12));
            }
        }
    }
    return $license_string;
}
