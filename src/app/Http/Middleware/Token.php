<?php

namespace App\Http\Middleware;

use Closure;
use Modules\Licence\Models\Licence;

class Token
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check DB exits
        if(!\Schema::hasTable('users')){
            $data = [
                'message'  => trans('messages.common.databaseNotExist'),
            ];
            return response()->json($data, config('constants.HTTP_STATUS_CODE.NOT_FOUND'));
        }

        $token = Licence::all()->pluck('token')->toArray();

        if (!in_array($request->header('token'), $token)) {
            $data = [
                'message'  => trans('messages.common.canNotAccessAPI'),
            ];
            return response()->json($data, config('constants.HTTP_STATUS_CODE.PERMISSION_DENIED'));
        }

//        $token = config('constants.TOKEN');
//
//        if ($request->header('token') != $token) {
//            $data = [
//                'message'  => trans('messages.common.canNotAccessAPI'),
//            ];
//            return response()->json($data, config('constants.HTTP_STATUS_CODE.PERMISSION_DENIED'));
//        }

        return $next($request);
    }
}
