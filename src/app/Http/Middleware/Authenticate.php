<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param null $guards
     * @return string
     */
    public function handle($request, \Closure $next)
    {
        if (Auth::check()) {
            return $next($request)
                ->header('Access-Control-Allow-Origin', 'http://192.168.111.182:9000')
                ->header('Access-Control-Allow-Methods', '*')
                ->header('Access-Control-Allow-Credentials', 'true')
                ->header('Access-Control-Allow-Headers', 'X-CSRF-Token');
        }
        return redirect('/login');
    }
}
