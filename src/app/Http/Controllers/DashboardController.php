<?php

namespace App\Http\Controllers;

use ArielMejiaDev\LarapexCharts\LarapexChart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Camera\Models\Camera;
use Modules\ObjectAppearance\Models\ObjectAppearance;
use Modules\ObjectAppearance\Models\Report;

class DashboardController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $requests = $request->all();

        $viewBy = $request->view_by;
        $viewTime = $request->view_time;
        $time = explode('/', $viewTime);
    
        $flagAllCamera = false;
        $checkAll = [];
        if (request()->object_id && !request()->camera_id){
            $cameras = Camera::where('object_appearance_id', $requests['object_id'])->pluck('id')->toArray();
            if (count($cameras)) {
                $checkAll = $cameras;
                $flagAllCamera = true;
            }
        }

        // case when click object on menu manager objects
//        if (isset($requests['object_id']) && isset($requests['view_by']) && isset($requests['view_time'])) {
//            $object = ObjectAppearance::findOrFail($requests['object_id']);
//            $cameraObject = $object->cameras->pluck('id')->toArray();
//
//            $dataNull = false;
//            switch ($viewBy) {
//                case Report::VIEW_DATE:
//                    $reportsFilter = Report::where(['date' => $time[0], 'month' => $time[1], 'year' => $time[2]])->whereIn('camera_id', $cameraObject)->get();
//
//                    if ($reportsFilter->count() === 0) {
//                        $dataNull = true;
//                    }
//
//                    for ($x = 0; $x <= 23; $x++) {
//                        $collections[] = "$x" . ' Giờ';
//                        $haveMask[$x] = 0;
//                        $noMask[$x] = 0;
//                        foreach ($reportsFilter as $value) {
//                            if ($value->hours == $x) {
//                                $resultEntering = $reportsFilter->where('hours', $x)->sum('people_entering');
//                                $resultHaveMask = $reportsFilter->where('hours', $x)->sum('people_have_mask');
//                                $resultNoMask = $reportsFilter->where('hours', $x)->sum('people_no_mask');
//                                $entering[$x] = $resultEntering;
//                                $haveMask[$x] = $resultHaveMask;
//                                $noMask[$x] = $resultNoMask;
//                            }
//                        }
//                    }
//
//                    break;
//
//                case Report::VIEW_MONTH:
//
//                    $reportsFilter = Report::where(['month' => $time[0], 'year' => $time[1]])->whereIn('camera_id', $cameraObject)->get();
//
//                    if ($reportsFilter->count() === 0) {
//                        $dataNull = true;
//                    }
//
//                    for ($x = 1; $x <= 31; $x++) {
//                        $collections[] = 'Ngày ' . $x;
//
//                        $entering[] = 0;
//                        $haveMask[] = 0;
//                        $noMask[] = 0;
//                        foreach ($reportsFilter as $value) {
//                            if ($value->date == $x) {
//                                $resultEntering = $reportsFilter->where('date', $x)->sum('people_entering');
//                                $resultHaveMask = $reportsFilter->where('date', $x)->sum('people_have_mask');
//                                $resultNoMask = $reportsFilter->where('date', $x)->sum('people_no_mask');
//                                $entering[$x-1] = $resultEntering;
//                                $haveMask[$x-1] = $resultHaveMask;
//                                $noMask[$x-1] = $resultNoMask;
//                            }
//                        }
//                    }
//
//                    break;
//
//                case Report::VIEW_YEAR:
//
//                    $reportsFilter = Report::where(['year' => $time[0]])->whereIn('camera_id', $cameraObject)->get();
//
//                    if ($reportsFilter->count() === 0) {
//                        $dataNull = true;
//                    }
//
//                    for ($x = 1; $x <= 12; $x++) {
//                        $collections[] = 'Tháng ' . $x;
//
//                        $entering[$x-1] = 0;
//                        $haveMask[$x-1] = 0;
//                        $noMask[$x-1] = 0;
//
//                        foreach ($reportsFilter as $value) {
//                            if ($value->month == $x) {
//                                $resultEntering = $reportsFilter->where('month', $x)->sum('people_entering');
//                                $resultHaveMask = $reportsFilter->where('month', $x)->sum('people_have_mask');
//                                $resultNoMask = $reportsFilter->where('month', $x)->sum('people_no_mask');
//                                $entering[$x-1] = $resultEntering;
//                                $haveMask[$x-1] = $resultHaveMask;
//                                $noMask[$x-1] = $resultNoMask;
//                            }
//                        }
//                    }
//
//                    break;
//            }
//
//            $chart = '';
//
//            if ($dataNull === false) {
//                $chart = (new LarapexChart)->areaChart()
//                    ->setTitle('Số lượng người')
//                    ->addData('Tổng người', $entering)
//                    ->addData('Đeo khẩu trang', $haveMask)
//                    ->addData('Không đeo khẩu trang', $noMask)
//                    ->setXAxis($collections)
//                    ->setColors(['#1d00ff', '#f00', '#ffeb3b']);
//
//            }
//
//            if (!Auth::user()->isAdmin()) {
//                $objects = [];
//                $object = Auth::user()->objectAppearance ? Auth::user()->objectAppearance->id : '';
//                $cameras = Camera::where('object_appearance_id', $object)->get();
//
//            } else {
//                $objects = ObjectAppearance::all();
//                $object = ObjectAppearance::findOrFail($requests['object_id']);
//                $cameras = $object->cameras;
//            }
//
//            return view('dashboard', compact('dataNull','chart','objects', 'cameras', 'checkAll'));
//        }

//        if (isset($requests['object_id']) && isset($requests['camera_id']) && isset($requests['view_by']) && isset($requests['view_time'])) {
//
//            $dataNull = false;
//
//            switch ($viewBy) {
//                case Report::VIEW_DATE:
//                    $reportsFilter = Report::where(['date' => $time[0], 'month' => $time[1], 'year' => $time[2], 'camera_id' => $request->camera_id])->get();
//
//                    if ($reportsFilter->count() === 0) {
//                        $dataNull = true;
//                    }
//
//                    for ($x = 0; $x <= 23; $x++) {
//                        $collections[] = "$x" . ' Giờ';
//                        $haveMask[$x] = 0;
//                        $noMask[$x] = 0;
//                        foreach ($reportsFilter as $value) {
//                            if ($value->hours == $x) {
//                                $resultEntering = $reportsFilter->where('hours', $x)->sum('people_entering');
//                                $resultHaveMask = $reportsFilter->where('hours', $x)->sum('people_have_mask');
//                                $resultNoMask = $reportsFilter->where('hours', $x)->sum('people_no_mask');
//                                $entering[$x] = $resultEntering;
//                                $haveMask[$x] = $resultHaveMask;
//                                $noMask[$x] = $resultNoMask;
//                            }
//                        }
//                    }
//
//                    break;
//
//                case Report::VIEW_MONTH:
//
//                    $reportsFilter = Report::where(['month' => $time[0], 'year' => $time[1], 'camera_id' => $request->camera_id])->get();
//
//                    if ($reportsFilter->count() === 0) {
//                        $dataNull = true;
//                    }
//
//                    for ($x = 1; $x <= 31; $x++) {
//                        $collections[] = 'Ngày ' . $x;
//
//                        $entering[] = 0;
//                        $haveMask[] = 0;
//                        $noMask[] = 0;
//                        foreach ($reportsFilter as $value) {
//                            if ($value->date == $x) {
//                                $resultEntering = $reportsFilter->where('date', $x)->sum('people_entering');
//                                $resultHaveMask = $reportsFilter->where('date', $x)->sum('people_have_mask');
//                                $resultNoMask = $reportsFilter->where('date', $x)->sum('people_no_mask');
//                                $entering[$x-1] = $resultEntering;
//                                $haveMask[$x-1] = $resultHaveMask;
//                                $noMask[$x-1] = $resultNoMask;
//                            }
//                        }
//                    }
//
//                    break;
//
//                case Report::VIEW_YEAR:
//
//                    $reportsFilter = Report::where(['year' => $time[0], 'camera_id' => $request->camera_id])->get();
//
//                    if ($reportsFilter->count() === 0) {
//                        $dataNull = true;
//                    }
//
//                    for ($x = 1; $x <= 12; $x++) {
//                        $collections[] = 'Tháng ' . $x;
//
//                        $entering[$x-1] = 0;
//                        $haveMask[$x-1] = 0;
//                        $noMask[$x-1] = 0;
//
//                        foreach ($reportsFilter as $value) {
//                            if ($value->month == $x) {
//                                $resultEntering = $reportsFilter->where('month', $x)->sum('people_entering');
//                                $resultHaveMask = $reportsFilter->where('month', $x)->sum('people_have_mask');
//                                $resultNoMask = $reportsFilter->where('month', $x)->sum('people_no_mask');
//                                $entering[$x-1] = $resultEntering;
//                                $haveMask[$x-1] = $resultHaveMask;
//                                $noMask[$x-1] = $resultNoMask;
//                            }
//                        }
//                    }
//
//                    break;
//            }
//
//            $chart = '';
//
//            if ($dataNull === false) {
//                $chart = (new LarapexChart)->areaChart()
//                    ->setTitle('Số lượng người')
//                    ->addData('Tổng người', $entering)
//                    ->addData('Đeo khẩu trang', $haveMask)
//                    ->addData('Không đeo khẩu trang', $noMask)
//                    ->setXAxis($collections)
//                    ->setColors(['#1d00ff', '#f00', '#ffeb3b']);
//
//                $objects = [];
//
//            }
//
//            if (!Auth::user()->isAdmin()) {
//                $objects = [];
//                $object = Auth::user()->objectAppearance ? Auth::user()->objectAppearance->id : '';
//                $cameras = Camera::where('object_appearance_id', $object)->get();
//
//            } else {
//                $objects = ObjectAppearance::all();
//                $cameras = Camera::all();
//            }
//
//            return view('dashboard', compact('dataNull','chart','objects', 'cameras'));
//        }

//        for ($x = 0; $x <= 24; $x++) {
//            $collections[] = "$x" . ' Giờ';
//            $collectionsAll[] = '0';
//            $collectionsHaveMask[] = '0';
//            $collectionsNoMask[] = '0';
//        }
//
        $chart = [];

        if ( !Auth::user()->isAdmin()) {
            $user = Auth::user();
            $objects = $user->objectAppearance()->with(['cameras' => function ($query) {
                $query->with('reports');
            }])->get();

            $objectSelects = [];
            foreach ($objects as $value) {
                $objectSelects[] = $value->pivot->object_appearance_id;
            }
            $cameras = Camera::whereIn('object_appearance_id', $objectSelects)->get();
        } else {
            $objects = ObjectAppearance::all();

            if (isset($requests['object_id'])) {
                $cameras = Camera::where('object_appearance_id', $requests['object_id'])->get();
            } else {
                $cameras = Camera::get();
            }
        }

        return view('dashboard', compact('chart','objects', 'cameras', 'checkAll','flagAllCamera'));

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home(Request $request)
    {
        $requests = $request->all();

        $viewBy = $request->view_by;
        $viewTime = $request->view_time;
        $time = explode('/', $viewTime);

        // case when click object on menu manager objects
//        if (isset($requests['view_by']) && isset($requests['view_time'])) {
//            $object = ObjectAppearance::findOrFail($requests['object_id']);
//            $cameraObject = $object->cameras->pluck('id')->toArray();
//
//            $dataNull = false;
//            switch ($viewBy) {
//                case Report::VIEW_MONTH:
//
//                    $reportsFilter = Report::where(['month' => $time[0], 'year' => $time[1]])->whereIn('camera_id', $cameraObject)->get();
//
//                    if ($reportsFilter->count() === 0) {
//                        $dataNull = true;
//                    }
//
//                    for ($x = 1; $x <= 31; $x++) {
//                        $collections[] = 'Ngày ' . $x;
//
//                        $entering[] = 0;
//                        $haveMask[] = 0;
//                        $noMask[] = 0;
//                        foreach ($reportsFilter as $value) {
//                            if ($value->date == $x) {
//                                $resultEntering = $reportsFilter->where('date', $x)->sum('people_entering');
//                                $resultHaveMask = $reportsFilter->where('date', $x)->sum('people_have_mask');
//                                $resultNoMask = $reportsFilter->where('date', $x)->sum('people_no_mask');
//                                $entering[$x-1] = $resultEntering;
//                                $haveMask[$x-1] = $resultHaveMask;
//                                $noMask[$x-1] = $resultNoMask;
//                            }
//                        }
//                    }
//
//                    break;
//
//                case Report::VIEW_YEAR:
//
//                    $reportsFilter = Report::where(['year' => $time[0]])->whereIn('camera_id', $cameraObject)->get();
//
//                    if ($reportsFilter->count() === 0) {
//                        $dataNull = true;
//                    }
//
//                    for ($x = 1; $x <= 12; $x++) {
//                        $collections[] = 'Tháng ' . $x;
//
//                        $entering[$x-1] = 0;
//                        $haveMask[$x-1] = 0;
//                        $noMask[$x-1] = 0;
//
//                        foreach ($reportsFilter as $value) {
//                            if ($value->month == $x) {
//                                $resultEntering = $reportsFilter->where('month', $x)->sum('people_entering');
//                                $resultHaveMask = $reportsFilter->where('month', $x)->sum('people_have_mask');
//                                $resultNoMask = $reportsFilter->where('month', $x)->sum('people_no_mask');
//                                $entering[$x-1] = $resultEntering;
//                                $haveMask[$x-1] = $resultHaveMask;
//                                $noMask[$x-1] = $resultNoMask;
//                            }
//                        }
//                    }
//
//                    break;
//            }
//
//            $chart = '';
//
//            if ($dataNull === false) {
//                $chart = (new LarapexChart)->areaChart()
//                    ->setTitle('Số lượng người')
//                    ->addData('Tổng người', $entering)
//                    ->addData('Đeo khẩu trang', $haveMask)
//                    ->addData('Không đeo khẩu trang', $noMask)
//                    ->setXAxis($collections)
//                    ->setColors(['#1d00ff', '#f00', '#ffeb3b']);
//
//            }
//
//            if (!Auth::user()->isAdmin()) {
//                $objects = [];
//                $object = Auth::user()->objectAppearance ? Auth::user()->objectAppearance->id : '';
//                $cameras = Camera::where('object_appearance_id', $object)->get();
//
//            } else {
//                $objects = ObjectAppearance::all();
//                $object = ObjectAppearance::findOrFail($requests['object_id']);
//                $cameras = $object->cameras;
//            }
//
//            return view('dashboard', compact('dataNull','chart','objects', 'cameras'));
//        }

//        for ($x = 0; $x <= 23; $x++) {
//            $collections[] = "$x" . ' Giờ';
//            $collectionsAll[] = '0';
//            $collectionsHaveMask[] = '0';
//            $collectionsNoMask[] = '0';
//        }


        $chart = [];

        if ( !Auth::user()->isAdmin()) {
            $user = Auth::user();
            $objects = $user->objectAppearance()->with(['cameras' => function ($query) {
                $query->with('reports');
            }])->get();

            $objectSelects = [];
            foreach ($objects as $value) {
                $objectSelects[] = $value->pivot->object_appearance_id;
            }
            $cameras = Camera::whereIn('object_appearance_id', $objectSelects);
        } else {
            $objects = ObjectAppearance::all();
            $cameras = Camera::all();
        }

        return view('home', compact('chart','objects', 'cameras'));

    }

    /**
     * @param $idObjects
     */
    public function getCamera($idObjects)
    {
        $cameras = Camera::where('object_appearance_id', $idObjects)->get();

        foreach ($cameras as $value){
            echo "<option value='".$value->id."'>".$value->name."</option>";
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewChart(Request $request)
    {
        $requests = $request->all();
        $user = Auth::user();
        
        if(!Auth::check()){
           return 'FALSE';
        }

        if (empty($requests['object_id']) || empty($requests['camera_id']) || empty($requests['view_by']) || empty($requests['view_time'])) {
            return "<div class='no_content'><h3>Không có dữ liệu!</h3></div>";
        }

        $collections = [];
        $viewBy = $request->view_by;
        $viewTime = $request->view_time;
        $time = explode('/', $viewTime);

        $entering = [];
        $haveMask = [];
        $noMask = [];
        $reportByCamera = [];
    
        $reportByObject = [];
    
        $cameraIds = implode(',',$request->camera_id);
        
        switch ($viewBy) {
            case Report::VIEW_DATE:
                $reportsFilter = \DB::select(\DB::raw( "select object_appearances.id as object_id , sum(people_entering) as entering, sum(people_no_mask) as no_mask,
                        sum(people_have_mask) as have_mask, `month`, `year`, `camera_id`, `date`, `hours`
                            from `reports`, cameras, object_appearances
                            where reports.camera_id = cameras.id and object_appearances.id = cameras.object_appearance_id
                            and (`date` = :date and `month` = :month and `year` = :year)
                            and reports.camera_id in (" . $cameraIds .")
                            group by `month`, `year`, `object_id`, `date`, `camera_id`, `hours`"),
                    ['date' => $time[0], 'month' =>  $time[1], 'year' =>  $time[2]]);
                

                if (count($reportsFilter) == 0) {
                    return "<div class='no_content'><h3>Không có dữ liệu!</h3></div>";
                }

                for ($x = 0; $x <= 23; $x++) {
                    $collections[] = "$x" . ' Giờ';
                    $entering[$x] = 0;
                    $haveMask[$x] = 0;
                    $noMask[$x] = 0;
    
                    $dateEntering = 0;
                    $dataHaveMask = 0;
                    $dataNoMask = 0;
                    
                    foreach ($reportsFilter as $value) {
                        if ($value->hours == $x) {
                            //report table
                            $resultEntering = $value->entering;
                            $resultHaveMask = $value->have_mask;
                            $resultNoMask = $value->no_mask;

                            $entering[$x] = $resultEntering;
                            $haveMask[$x] = $resultHaveMask;
                            $noMask[$x] = $resultNoMask;
    
                            $reportByCamera[$value->camera_id]['entering'][$x] = $resultEntering;
                            $reportByCamera[$value->camera_id]['have_mask'][$x] = $resultHaveMask;
                            $reportByCamera[$value->camera_id]['no_mask'][$x] = $resultNoMask;
    
    
                            // total chart
                            $dateEntering += $value->entering;
                            $dataHaveMask += $value->have_mask;
                            $dataNoMask += $value->no_mask;
                        }
    
                        $reportByObject['entering'][$x+1] = $dateEntering;
                        $reportByObject['have_mask'][$x+1] = $dataHaveMask;
                        $reportByObject['no_mask'][$x+1] = $dataNoMask;
                        
                        $reportByCamera[$value->camera_id]['collections'] = $collections;
                    }
                }

                $chart = $this->dataChart($collections, array_values($reportByObject['entering']) , array_values($reportByObject['have_mask']), array_values($reportByObject['no_mask']));

                break;

            case Report::VIEW_MONTH:

                $reportsFilter = \DB::select(\DB::raw( "select object_appearances.id as object_id , sum(people_entering) as entering, sum(people_no_mask) as no_mask,
                        sum(people_have_mask) as have_mask, `month`, `year`, `camera_id`, `date`
                            from `reports`, cameras, object_appearances
                            where reports.camera_id = cameras.id and object_appearances.id = cameras.object_appearance_id
                            and (`month` = :month and `year` = :year)
                            and reports.camera_id in (" . $cameraIds .")
                            group by `month`, `year`, `object_id`, `date`, `camera_id`"),
                    ['month' =>  $time[0], 'year' =>  $time[1]]);

                if (count($reportsFilter) == 0) {
                    return "<div class='no_content'><h3>Không có dữ liệu!</h3></div>";
                }

                for ($x = 1; $x <= 31; $x++) {
                    $collections[$x] = 'Ngày ' . $x;

                    $entering[] = 0;
                    $haveMask[] = 0;
                    $noMask[] = 0;
                    
                    $dateEntering = 0;
                    $dataHaveMask = 0;
                    $dataNoMask = 0;
                    
                    foreach ($reportsFilter as $value) {
                        if ($value->date == $x) {
                            //report table
                            $resultEntering = $value->entering;
                            $resultHaveMask = $value->have_mask;
                            $resultNoMask = $value->no_mask;
                            
                            $entering[$x-1] = $resultEntering;
                            $haveMask[$x-1] = $resultHaveMask;
                            $noMask[$x-1] = $resultNoMask;
                            
                            $reportByCamera[$value->camera_id]['entering'][$x] = $resultEntering;
                            $reportByCamera[$value->camera_id]['have_mask'][$x] = $resultHaveMask;
                            $reportByCamera[$value->camera_id]['no_mask'][$x] = $resultNoMask;

                            // total chart
                            $dateEntering += $value->entering;
                            $dataHaveMask += $value->have_mask;
                            $dataNoMask += $value->no_mask;
                            
                        }
                        $reportByObject['entering'][$x+1] = $dateEntering;
                        $reportByObject['have_mask'][$x+1] = $dataHaveMask;
                        $reportByObject['no_mask'][$x+1] = $dataNoMask;
                        
                        $reportByCamera[$value->camera_id]['collections'] = $collections;
                    }
                }

                $chart = $this->dataChart($collections, array_values($reportByObject['entering']) , array_values($reportByObject['have_mask']), array_values($reportByObject['no_mask']));
                break;

            case Report::VIEW_YEAR:
    
                $reportsFilter = \DB::select(\DB::raw( "select object_appearances.id as object_id , sum(people_entering) as entering, sum(people_no_mask) as no_mask,
                        sum(people_have_mask) as have_mask, `month`, `year`, `camera_id`, `date`
                            from `reports`, cameras, object_appearances
                            where reports.camera_id = cameras.id and object_appearances.id = cameras.object_appearance_id
                            and (`year` = :year)
                            and reports.camera_id in (" . $cameraIds .")
                            group by `month`, `year`, `object_id`, `date`, `camera_id`"),
                    ['year' =>  $time[0]]);

                if (count($reportsFilter) == 0) {
                    return "<div class='no_content'><h3>Không có dữ liệu!</h3></div>";
                }

                for ($x = 1; $x <= 12; $x++) {
                    $collections[$x] = 'Tháng ' . $x;

                    $entering[$x-1] = 0;
                    $haveMask[$x-1] = 0;
                    $noMask[$x-1] = 0;
    
                    $dateEntering = 0;
                    $dataHaveMask = 0;
                    $dataNoMask = 0;

                    foreach ($reportsFilter as $value) {
                        if ($value->month == $x) {
                            //report table
                            $resultEntering = $value->entering;
                            $resultHaveMask = $value->have_mask;
                            $resultNoMask = $value->no_mask;
                            
                            $entering[$x-1] = $resultEntering;
                            $haveMask[$x-1] = $resultHaveMask;
                            $noMask[$x-1] = $resultNoMask;
    
                            $reportByCamera[$value->camera_id]['entering'][$x] = $resultEntering;
                            $reportByCamera[$value->camera_id]['have_mask'][$x] = $resultHaveMask;
                            $reportByCamera[$value->camera_id]['no_mask'][$x] = $resultNoMask;
    
                            // total chart
                            $dateEntering += $value->entering;
                            $dataHaveMask += $value->have_mask;
                            $dataNoMask += $value->no_mask;

                        }
    
                        $reportByObject['entering'][$x+1] = $dateEntering;
                        $reportByObject['have_mask'][$x+1] = $dataHaveMask;
                        $reportByObject['no_mask'][$x+1] = $dataNoMask;
                        
                        $reportByCamera[$value->camera_id]['collections'] = $collections;
                    }
                }

                $chart = $this->dataChart($collections, array_values($reportByObject['entering']) , array_values($reportByObject['have_mask']), array_values($reportByObject['no_mask']));

                break;
        }


        $cameras = Camera::whereIn('id', $request->camera_id)->get();

        // PieChart
        $pieChart = (new LarapexChart)->pieChart()
            ->addData([array_sum($reportByObject['have_mask']), array_sum($reportByObject['no_mask'])])
            ->setColors(['#42A5F5','#ef5350'])
            ->setLabels(['Đeo khẩu trang', 'Không đeo khẩu trang']);

        $time = $request->view_time;

        return view('pages.chart.chart', compact('chart', 'cameras', 'pieChart', 'reportByCamera', 'time'));

    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function viewChartHome(Request $request)
    {
        $requests = $request->all();

        if(!Auth::check()){
            return 'FALSE';
        }

        if (empty($requests['view_by']) || empty($requests['view_time'])) {
            return "<div class='no_content'><h3>Không có dữ liệu!</h3></div>";
        }

        $collections = [];
        $viewBy = $request->view_by;
        $viewTime = $request->view_time;
        $time = explode('/', $viewTime);

        $reportByObject = [];
    
        $user = Auth::user();
        
        switch ($viewBy) {
            case Report::VIEW_MONTH:

                if ( !Auth::user()->isAdmin()) {
                    $objects = $user->objectAppearance;
                    if (count($objects) == 0) {
                        return "<div class='no_content'><h3>Không có dữ liệu!</h3></div>";
                    }
                    $valueObject = implode(',',$objects->pluck('id')->toArray());

                } else {
                    $objects = ObjectAppearance::select('id', 'name')->get();
                    if (count($objects) == 0) {
                        return "<div class='no_content'><h3>Không có dữ liệu!</h3></div>";
                    }
                    $valueObject = implode(',',$objects->pluck('id')->toArray());
                }

                $reportsFilter = \DB::select(\DB::raw( "select object_appearances.id as object_id , sum(people_entering) as entering, sum(people_no_mask) as no_mask, sum(people_have_mask) as have_mask, `month`, `year`, `date`
                            from `reports`, cameras, object_appearances
                            where reports.camera_id = cameras.id and object_appearances.id = cameras.object_appearance_id
                            and (`month` = :month and `year` = :year)
                            and object_appearances.id in (" . $valueObject .")
                            group by `month`, `year`, `object_id`, `date`"),
                    ['month' =>  $time[0], 'year' =>  $time[1]]);

                if (count($reportsFilter) == 0) {
                    return "<div class='no_content'><h3>Không có dữ liệu!</h3></div>";
                }
                

                $dataReport = [];
                foreach ($objects as $value) {
                    foreach ($reportsFilter as $item) {
                        if ($item->object_id == $value->id) {
                            $dataReport[$item->object_id][] = $item;
                        }
                    }
                }

                for ($x = 1; $x <= 31; $x++) {
                    $collections[$x] = 'Ngày ' . $x;
                    $entering[] = 0;
                    $haveMask[] = 0;
                    $noMask[] = 0;
                    
                    foreach ($dataReport as $value) {
                        $resultEntering = 0;
                        $resultNoMask = 0;
                        foreach ($value as $item) {
                            if ($item->date == $x) {
                                $resultEntering += $item->entering;
                                $resultNoMask += $item->no_mask;
                            }

                            $reportByObject[$item->object_id]['entering'][$x-1] = $resultEntering;
                            $reportByObject[$item->object_id]['no_mask'][$x-1] = $resultNoMask;
                        }
                    }
                }
                break;

            case Report::VIEW_YEAR:
                if ( !Auth::user()->isAdmin()) {
                    $objects = $user->objectAppearance;
                    if (count($objects) == 0) {
                        return "<div class='no_content'><h3>Không có dữ liệu!</h3></div>";
                    }
                    $valueObject = implode(',',$objects->pluck('id')->toArray());
                } else {
                    $objects = ObjectAppearance::select('id', 'name')->get();
                    if (count($objects) == 0) {
                        return "<div class='no_content'><h3>Không có dữ liệu!</h3></div>";
                    }
                    $valueObject = implode(',',$objects->pluck('id')->toArray());
                }
    
    
                $reportsFilter = \DB::select(\DB::raw( "select object_appearances.id as object_id , sum(people_entering) as entering,
                            sum(people_no_mask) as no_mask, sum(people_have_mask) as have_mask, `month`, `year`
                            from `reports`, cameras, object_appearances
                            where reports.camera_id = cameras.id and object_appearances.id = cameras.object_appearance_id
                            and `year` = :year
                            and object_appearances.id in (" . $valueObject .")
                            group by `month`, `year`, `object_id`"),
                    ['year' =>  $time[0]]);
                
                if (count($reportsFilter) == 0) {
                    return "<div class='no_content'><h3>Không có dữ liệu!</h3></div>";
                }
    
                $dataReport = [];
                foreach ($objects as $value) {
                    foreach ($reportsFilter as $item) {
                        if ($item->object_id == $value->id) {
                            $dataReport[$item->object_id][] = $item;
                        }
                    }
                }

                for ($x = 1; $x <= 12; $x++) {
                    $collections[$x] = 'Tháng ' . $x;
                    foreach ($dataReport as $value) {
                        $resultEntering = 0;
                        $resultNoMask = 0;
                        foreach ($value as $item) {
                            if ($item->month == $x) {
                                $resultEntering += $item->entering;
                                $resultNoMask += $item->no_mask;
                            }
                        }
                        $reportByObject[$item->object_id]['entering'][$x] = $resultEntering;
                        $reportByObject[$item->object_id]['no_mask'][$x] = $resultNoMask;
                    }
                }

                break;
        }
 

//        $objects = ObjectAppearance::with('cameras')->get();

        $count = count($objects);
        $colors = $this->generateColorChart($count);

        $chartFirst = (new LarapexChart)->lineChart()
            ->setTitle('Số lượng người vào')
            ->setXAxis($collections)
            ->setColors($colors);

        $chartSecond = (new LarapexChart)->lineChart()
            ->setTitle('Số lượng người không đeo khẩu trang')
            ->setXAxis($collections)
            ->setColors($colors);

        foreach ($objects as $key => $value) {
            if (array_key_exists($value->id, $reportByObject)) {
                $chartFirst->addData($value->name, array_values($reportByObject[$value->id]['entering']));
                $chartSecond->addData($value->name, array_values($reportByObject[$value->id]['no_mask']));
            }
        }

        return view('pages.chart.chart_home', compact('chartFirst', 'chartSecond'));
    }
    
    /**
     * @return array
     */
    function array_merge_numeric_values()
    {
        $arrays = func_get_args();

        $merged = array();
        foreach ($arrays[0] as $array)
        {
            foreach ($array as $key => $value)
            {
                if ( ! is_numeric($value))
                {
                    continue;
                }
                if ( ! isset($merged[$key]))
                {
                    $merged[$key] = $value;
                }
                else
                {
                    $merged[$key] += $value;
                }
            }
        }
        return $merged;
    }

    /**
     * @param $collections
     * @param $entering
     * @param $haveMask
     * @param $noMask
     * @return mixed
     */
    public function dataChart($collections, $entering, $haveMask, $noMask)
    {

        $chart = (new LarapexChart)->areaChart()
            ->setTitle('Số lượng người')
            ->addData('Tổng người', $entering)
            ->addData('Đeo khẩu trang', $haveMask)
            ->addData('Không đeo khẩu trang', $noMask)
            ->setXAxis($collections)
            ->setColors(['#D4E157','#42A5F5','#ef5350']);

        return $chart;
    }

    /**
     * @return string
     */
    public function random_color_part()
    {
        return str_pad(dechex(mt_rand(128, 255)), 2, '0', STR_PAD_LEFT);
    }

    /**
     * @return string
     */
    public function random_color() {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }

    /**
     * @param $number
     * @return array
     */
    private function generateColorChart($number)
    {
        $color = [];
        for ($i = 1; $i <= $number; $i++) {
            $color[] = '#' . $this->random_color();
        }

        return $color;
    }

}
