$(function() {

    showSwal = function(type) {
        'use strict';
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false,
        })

        swalWithBootstrapButtons.fire({
            title: 'Bạn chắc chắn muốn xóa?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'ml-2',
            confirmButtonText: 'Có',
            cancelButtonText: 'Không!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });

                $.ajax({
                    url: ,
                    type: 'delete',
                    data: {
                        id: 5
                    },
                    dataType: "html",
                    success: function () {
                        swal("Done!", "It was succesfully deleted!", "success");
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error deleting!", "Please try again", "error");
                    }
                });

                swalWithBootstrapButtons.fire(
                    'Giấy phép đã được xóa',
                    '',
                    'success'
                )
            } else if (
                // Read more about handling dismissals
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                    'Hủy xóa giấy phép',
                    '',
                    'error'
                )
            }
        })
    }

});
