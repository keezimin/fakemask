(function () {

    var laroute = (function () {

        var routes = {

            absolute: false,
            rootUrl: 'http://localhost',
            routes : [{"host":null,"methods":["GET","POST","HEAD"],"uri":"broadcasting\/auth","name":null,"action":"\Illuminate\Broadcasting\BroadcastController@authenticate"},{"host":null,"methods":["GET","HEAD"],"uri":"objects","name":"objects","action":"Modules\ObjectAppearance\Controllers\ObjectAppearanceController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"objects\/{id}\/cameras","name":"cameraByObject","action":"Modules\ObjectAppearance\Controllers\ObjectAppearanceController@cameraByObject"},{"host":null,"methods":["POST"],"uri":"api\/objects\/sync","name":null,"action":"Modules\ObjectAppearance\Controllers\ObjectAppearanceController@sync"},{"host":null,"methods":["POST"],"uri":"api\/objects\/get_latest_result_sync","name":null,"action":"Modules\ObjectAppearance\Controllers\ObjectAppearanceController@getLatestResultSync"},{"host":null,"methods":["POST"],"uri":"api\/objects\/store","name":null,"action":"Modules\ObjectAppearance\Controllers\ObjectAppearanceController@store"},{"host":null,"methods":["POST"],"uri":"api\/cameras","name":null,"action":"Modules\Camera\Controllers\CameraController@store"},{"host":null,"methods":["PATCH"],"uri":"api\/cameras\/{id}","name":null,"action":"Modules\Camera\Controllers\CameraController@update"},{"host":null,"methods":["DELETE"],"uri":"api\/cameras\/{id}","name":null,"action":"Modules\Camera\Controllers\CameraController@delete"},{"host":null,"methods":["GET","HEAD"],"uri":"users","name":"users","action":"Modules\User\Controllers\UserController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"users\/create","name":"users.create","action":"Modules\User\Controllers\UserController@create"},{"host":null,"methods":["POST"],"uri":"users\/create","name":"users.store","action":"Modules\User\Controllers\UserController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"users\/edit\/{id}","name":"users.edit","action":"Modules\User\Controllers\UserController@edit"},{"host":null,"methods":["POST"],"uri":"users\/edit\/{id}","name":"users.update","action":"Modules\User\Controllers\UserController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"users\/delete\/{id}","name":"users.delete","action":"Modules\User\Controllers\UserController@delete"},{"host":null,"methods":["GET","HEAD"],"uri":"licences","name":"licences.index","action":"Modules\Licence\Controllers\LicenceController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"licences\/create","name":"licences.create","action":"Modules\Licence\Controllers\LicenceController@create"},{"host":null,"methods":["POST"],"uri":"licences","name":"licences.deleteLicenceAjax","action":"Modules\Licence\Controllers\LicenceController@deleteLicenceAjax"},{"host":null,"methods":["GET","HEAD"],"uri":"licences\/{licence}","name":"licences.show","action":"Modules\Licence\Controllers\LicenceController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"licences\/{licence}\/edit","name":"licences.edit","action":"Modules\Licence\Controllers\LicenceController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"licences\/{licence}","name":"licences.update","action":"Modules\Licence\Controllers\LicenceController@update"},{"host":null,"methods":["DELETE"],"uri":"licences\/{licence}","name":"licences.destroy","action":"Modules\Licence\Controllers\LicenceController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"api\/user","name":null,"action":"Closure"},{"host":null,"methods":["GET","HEAD"],"uri":"login","name":"login","action":"\App\Http\Controllers\Auth\LoginController@showLoginForm"},{"host":null,"methods":["POST"],"uri":"login","name":null,"action":"\App\Http\Controllers\Auth\LoginController@login"},{"host":null,"methods":["POST"],"uri":"logout","name":"logout","action":"\App\Http\Controllers\Auth\LoginController@logout"},{"host":null,"methods":["GET","HEAD"],"uri":"password\/reset","name":"password.request","action":"\App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm"},{"host":null,"methods":["POST"],"uri":"password\/email","name":"password.email","action":"\App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail"},{"host":null,"methods":["GET","HEAD"],"uri":"password\/reset\/{token}","name":"password.reset","action":"\App\Http\Controllers\Auth\ResetPasswordController@showResetForm"},{"host":null,"methods":["POST"],"uri":"password\/reset","name":"password.update","action":"\App\Http\Controllers\Auth\ResetPasswordController@reset"},{"host":null,"methods":["GET","HEAD"],"uri":"password\/confirm","name":"password.confirm","action":"\App\Http\Controllers\Auth\ConfirmPasswordController@showConfirmForm"},{"host":null,"methods":["POST"],"uri":"password\/confirm","name":null,"action":"\App\Http\Controllers\Auth\ConfirmPasswordController@confirm"},{"host":null,"methods":["GET","HEAD"],"uri":"analytics","name":"dashboard.index","action":"\App\Http\Controllers\DashboardController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"\/","name":"home","action":"\App\Http\Controllers\DashboardController@home"},{"host":null,"methods":["GET","HEAD"],"uri":"get_cameras\/{id}","name":"getCameras","action":"\App\Http\Controllers\DashboardController@getCamera"},{"host":null,"methods":["GET","HEAD"],"uri":"view_chart","name":"viewChart","action":"\App\Http\Controllers\DashboardController@viewChart"},{"host":null,"methods":["GET","HEAD"],"uri":"view_chart\/home","name":"viewChartHome","action":"\App\Http\Controllers\DashboardController@viewChart"}],
            prefix: '',

            route : function (name, parameters, route) {
                route = route || this.getByName(name);

                if ( ! route ) {
                    return undefined;
                }

                return this.toRoute(route, parameters);
            },

            url: function (url, parameters) {
                parameters = parameters || [];

                var uri = url + '/' + parameters.join('/');

                return this.getCorrectUrl(uri);
            },

            toRoute : function (route, parameters) {
                var uri = this.replaceNamedParameters(route.uri, parameters);
                var qs  = this.getRouteQueryString(parameters);

                if (this.absolute && this.isOtherHost(route)){
                    return "//" + route.host + "/" + uri + qs;
                }

                return this.getCorrectUrl(uri + qs);
            },

            isOtherHost: function (route){
                return route.host && route.host != window.location.hostname;
            },

            replaceNamedParameters : function (uri, parameters) {
                uri = uri.replace(/\{(.*?)\??\}/g, function(match, key) {
                    if (parameters.hasOwnProperty(key)) {
                        var value = parameters[key];
                        delete parameters[key];
                        return value;
                    } else {
                        return match;
                    }
                });

                // Strip out any optional parameters that were not given
                uri = uri.replace(/\/\{.*?\?\}/g, '');

                return uri;
            },

            getRouteQueryString : function (parameters) {
                var qs = [];
                for (var key in parameters) {
                    if (parameters.hasOwnProperty(key)) {
                        qs.push(key + '=' + parameters[key]);
                    }
                }

                if (qs.length < 1) {
                    return '';
                }

                return '?' + qs.join('&');
            },

            getByName : function (name) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].name === name) {
                        return this.routes[key];
                    }
                }
            },

            getByAction : function(action) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].action === action) {
                        return this.routes[key];
                    }
                }
            },

            getCorrectUrl: function (uri) {
                var url = this.prefix + '/' + uri.replace(/^\/?/, '');

                if ( ! this.absolute) {
                    return url;
                }

                return this.rootUrl.replace('/\/?$/', '') + url;
            }
        };

        var getLinkAttributes = function(attributes) {
            if ( ! attributes) {
                return '';
            }

            var attrs = [];
            for (var key in attributes) {
                if (attributes.hasOwnProperty(key)) {
                    attrs.push(key + '="' + attributes[key] + '"');
                }
            }

            return attrs.join(' ');
        };

        var getHtmlLink = function (url, title, attributes) {
            title      = title || url;
            attributes = getLinkAttributes(attributes);

            return '<a href="' + url + '" ' + attributes + '>' + title + '</a>';
        };

        return {
            // Generate a url for a given controller action.
            // laroute.action('HomeController@getIndex', [params = {}])
            action : function (name, parameters) {
                parameters = parameters || {};

                return routes.route(name, parameters, routes.getByAction(name));
            },

            // Generate a url for a given named route.
            // laroute.route('routeName', [params = {}])
            route : function (route, parameters) {
                parameters = parameters || {};

                return routes.route(route, parameters);
            },

            // Generate a fully qualified URL to the given path.
            // laroute.route('url', [params = {}])
            url : function (route, parameters) {
                parameters = parameters || {};

                return routes.url(route, parameters);
            },

            // Generate a html link to the given url.
            // laroute.link_to('foo/bar', [title = url], [attributes = {}])
            link_to : function (url, title, attributes) {
                url = this.url(url);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given route.
            // laroute.link_to_route('route.name', [title=url], [parameters = {}], [attributes = {}])
            link_to_route : function (route, title, parameters, attributes) {
                var url = this.route(route, parameters);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given controller action.
            // laroute.link_to_action('HomeController@getIndex', [title=url], [parameters = {}], [attributes = {}])
            link_to_action : function(action, title, parameters, attributes) {
                var url = this.action(action, parameters);

                return getHtmlLink(url, title, attributes);
            }

        };

    }).call(this);

    /**
     * Expose the class either via AMD, CommonJS or the global object
     */
    if (typeof define === 'function' && define.amd) {
        define(function () {
            return laroute;
        });
    }
    else if (typeof module === 'object' && module.exports){
        module.exports = laroute;
    }
    else {
        window.laroute = laroute;
    }

}).call(this);

