<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Modules\Camera\Models\Camera;
use Modules\ObjectAppearance\Models\Report;

class ReportSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 1000; $i++){
            $date = Carbon::now()->format('d');
            $month = Carbon::now()->format('m');
            $year = Carbon::now()->format('Y');
            $total = $this->totalPeople();

            $report = Report::create([
                'camera_id' => 1,
                'people_entering' => $total[0],
                'people_have_mask' => $total[1],
                'people_no_mask' => $total[2],
                'minutes' => $i,
                'hours' => $i,
                'date' => $date,
                'month' => $month,
                'year' => $year
            ]);
        }
    }
    
    private function randomPeople()
    {
        return rand(0,100);
    }
    
    private function totalPeople()
    {
        $haveMask = $this->randomPeople();
        $noMask = $this->randomPeople();
        $total = $noMask + $haveMask;
        return [$total, $haveMask, $noMask];
    }
}
