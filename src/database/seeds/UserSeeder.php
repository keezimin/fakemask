<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Modules\User\Models\User;

class UserSeeder extends Seeder
{
    use \App\Traits\RequestAPI;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 10)->create();

        $data = [
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => '123',
            'role' => User::ROLE_ADMIN,
        ];

        DB::table('users')->insert([
            'name' => $data['name'],
            'email' => $data['email'],
            'role' => $data['role'],
            'password' => Hash::make($data['password']),
            'created_at' => Carbon::now(),
        ]);
    }
}
